// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Fri Sep 11 15:25:03 2020
// Host        : devlaptop running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/bsilva/Documentos/UnB/Final-Project/digital_systems/ov7670mcs/ov7670minized_vga/zcu_cmos/zcu_cmos.srcs/sources_1/bd/OV7670_QVGA/ip/OV7670_QVGA_Image_Visualization_0_1_1/OV7670_QVGA_Image_Visualization_0_1_sim_netlist.v
// Design      : OV7670_QVGA_Image_Visualization_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu7ev-ffvc1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "OV7670_QVGA_Image_Visualization_0_1,Image_Visualization_v1_0,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "Image_Visualization_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module OV7670_QVGA_Image_Visualization_0_1
   (s_clk,
    i_en,
    i_wen,
    i_px,
    i_addr,
    o_mf,
    s00_axi_awaddr,
    s00_axi_awprot,
    s00_axi_awvalid,
    s00_axi_awready,
    s00_axi_wdata,
    s00_axi_wstrb,
    s00_axi_wvalid,
    s00_axi_wready,
    s00_axi_bresp,
    s00_axi_bvalid,
    s00_axi_bready,
    s00_axi_araddr,
    s00_axi_arprot,
    s00_axi_arvalid,
    s00_axi_arready,
    s00_axi_rdata,
    s00_axi_rresp,
    s00_axi_rvalid,
    s00_axi_rready,
    s00_axi_aclk,
    s00_axi_aresetn);
  input s_clk;
  input i_en;
  input i_wen;
  input [7:0]i_px;
  input [18:0]i_addr;
  output o_mf;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWADDR" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI, WIZ_DATA_WIDTH 32, WIZ_NUM_REG 6, SUPPORTS_NARROW_BURST 0, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 50000000, ID_WIDTH 0, ADDR_WIDTH 5, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, NUM_READ_OUTSTANDING 8, NUM_WRITE_OUTSTANDING 8, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN OV7670_QVGA_zynq_ultra_ps_e_0_0_pl_clk0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input [4:0]s00_axi_awaddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWPROT" *) input [2:0]s00_axi_awprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWVALID" *) input s00_axi_awvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI AWREADY" *) output s00_axi_awready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WDATA" *) input [31:0]s00_axi_wdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WSTRB" *) input [3:0]s00_axi_wstrb;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WVALID" *) input s00_axi_wvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI WREADY" *) output s00_axi_wready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BRESP" *) output [1:0]s00_axi_bresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BVALID" *) output s00_axi_bvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI BREADY" *) input s00_axi_bready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARADDR" *) input [4:0]s00_axi_araddr;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARPROT" *) input [2:0]s00_axi_arprot;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARVALID" *) input s00_axi_arvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI ARREADY" *) output s00_axi_arready;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RDATA" *) output [31:0]s00_axi_rdata;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RRESP" *) output [1:0]s00_axi_rresp;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RVALID" *) output s00_axi_rvalid;
  (* x_interface_info = "xilinx.com:interface:aximm:1.0 S00_AXI RREADY" *) input s00_axi_rready;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 S00_AXI_CLK CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_CLK, ASSOCIATED_BUSIF S00_AXI, ASSOCIATED_RESET s00_axi_aresetn, FREQ_HZ 50000000, PHASE 0.000, CLK_DOMAIN OV7670_QVGA_zynq_ultra_ps_e_0_0_pl_clk0, INSERT_VIP 0" *) input s00_axi_aclk;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 S00_AXI_RST RST" *) (* x_interface_parameter = "XIL_INTERFACENAME S00_AXI_RST, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;

  wire \<const0> ;
  wire [18:0]i_addr;
  wire i_en;
  wire [7:0]i_px;
  wire i_wen;
  wire o_mf;
  wire s00_axi_aclk;
  wire [4:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arready;
  wire s00_axi_arvalid;
  wire [4:0]s00_axi_awaddr;
  wire s00_axi_awready;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire s00_axi_wready;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire s_clk;

  assign s00_axi_bresp[1] = \<const0> ;
  assign s00_axi_bresp[0] = \<const0> ;
  assign s00_axi_rresp[1] = \<const0> ;
  assign s00_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  OV7670_QVGA_Image_Visualization_0_1_Image_Visualization_v1_0 U0
       (.S_AXI_ARREADY(s00_axi_arready),
        .S_AXI_AWREADY(s00_axi_awready),
        .S_AXI_WREADY(s00_axi_wready),
        .i_addr(i_addr[16:0]),
        .i_en(i_en),
        .i_px(i_px),
        .i_wen(i_wen),
        .o_mf(o_mf),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr[4:2]),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr[4:2]),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s_clk(s_clk));
endmodule

(* ORIG_REF_NAME = "Image_Visualization_v1_0" *) 
module OV7670_QVGA_Image_Visualization_0_1_Image_Visualization_v1_0
   (o_mf,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    i_addr,
    s00_axi_aclk,
    s_clk,
    i_px,
    i_en,
    i_wen,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output o_mf;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input [16:0]i_addr;
  input s00_axi_aclk;
  input s_clk;
  input [7:0]i_px;
  input i_en;
  input i_wen;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire [16:0]i_addr;
  wire i_en;
  wire [7:0]i_px;
  wire i_wen;
  wire o_mf;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire s_clk;

  OV7670_QVGA_Image_Visualization_0_1_Image_Visualization_v1_0_S00_AXI Image_Visualization_v1_0_S00_AXI_inst
       (.S_AXI_ARREADY(S_AXI_ARREADY),
        .S_AXI_AWREADY(S_AXI_AWREADY),
        .S_AXI_WREADY(S_AXI_WREADY),
        .i_addr(i_addr),
        .i_en(i_en),
        .i_px(i_px),
        .i_wen(i_wen),
        .o_mf(o_mf),
        .s00_axi_aclk(s00_axi_aclk),
        .s00_axi_araddr(s00_axi_araddr),
        .s00_axi_aresetn(s00_axi_aresetn),
        .s00_axi_arvalid(s00_axi_arvalid),
        .s00_axi_awaddr(s00_axi_awaddr),
        .s00_axi_awvalid(s00_axi_awvalid),
        .s00_axi_bready(s00_axi_bready),
        .s00_axi_bvalid(s00_axi_bvalid),
        .s00_axi_rdata(s00_axi_rdata),
        .s00_axi_rready(s00_axi_rready),
        .s00_axi_rvalid(s00_axi_rvalid),
        .s00_axi_wdata(s00_axi_wdata),
        .s00_axi_wstrb(s00_axi_wstrb),
        .s00_axi_wvalid(s00_axi_wvalid),
        .s_clk(s_clk));
endmodule

(* ORIG_REF_NAME = "Image_Visualization_v1_0_S00_AXI" *) 
module OV7670_QVGA_Image_Visualization_0_1_Image_Visualization_v1_0_S00_AXI
   (o_mf,
    S_AXI_WREADY,
    S_AXI_AWREADY,
    S_AXI_ARREADY,
    s00_axi_rdata,
    s00_axi_rvalid,
    s00_axi_bvalid,
    i_addr,
    s00_axi_aclk,
    s_clk,
    i_px,
    i_en,
    i_wen,
    s00_axi_awaddr,
    s00_axi_wdata,
    s00_axi_araddr,
    s00_axi_wvalid,
    s00_axi_awvalid,
    s00_axi_wstrb,
    s00_axi_arvalid,
    s00_axi_aresetn,
    s00_axi_bready,
    s00_axi_rready);
  output o_mf;
  output S_AXI_WREADY;
  output S_AXI_AWREADY;
  output S_AXI_ARREADY;
  output [31:0]s00_axi_rdata;
  output s00_axi_rvalid;
  output s00_axi_bvalid;
  input [16:0]i_addr;
  input s00_axi_aclk;
  input s_clk;
  input [7:0]i_px;
  input i_en;
  input i_wen;
  input [2:0]s00_axi_awaddr;
  input [31:0]s00_axi_wdata;
  input [2:0]s00_axi_araddr;
  input s00_axi_wvalid;
  input s00_axi_awvalid;
  input [3:0]s00_axi_wstrb;
  input s00_axi_arvalid;
  input s00_axi_aresetn;
  input s00_axi_bready;
  input s00_axi_rready;

  wire S_AXI_ARREADY;
  wire S_AXI_AWREADY;
  wire S_AXI_WREADY;
  wire aw_en_i_1_n_0;
  wire aw_en_reg_n_0;
  wire axi_arready0;
  wire axi_awready0;
  wire axi_awready_i_1_n_0;
  wire axi_bvalid_i_1_n_0;
  wire \axi_rdata[0]_i_2_n_0 ;
  wire \axi_rdata[0]_i_3_n_0 ;
  wire \axi_rdata[0]_i_4_n_0 ;
  wire \axi_rdata[16]_i_2_n_0 ;
  wire \axi_rdata[17]_i_2_n_0 ;
  wire \axi_rdata[18]_i_2_n_0 ;
  wire \axi_rdata[19]_i_2_n_0 ;
  wire \axi_rdata[1]_i_2_n_0 ;
  wire \axi_rdata[1]_i_3_n_0 ;
  wire \axi_rdata[1]_i_4_n_0 ;
  wire \axi_rdata[20]_i_2_n_0 ;
  wire \axi_rdata[21]_i_2_n_0 ;
  wire \axi_rdata[22]_i_2_n_0 ;
  wire \axi_rdata[23]_i_2_n_0 ;
  wire \axi_rdata[24]_i_2_n_0 ;
  wire \axi_rdata[25]_i_2_n_0 ;
  wire \axi_rdata[26]_i_2_n_0 ;
  wire \axi_rdata[27]_i_2_n_0 ;
  wire \axi_rdata[28]_i_2_n_0 ;
  wire \axi_rdata[29]_i_2_n_0 ;
  wire \axi_rdata[2]_i_2_n_0 ;
  wire \axi_rdata[2]_i_3_n_0 ;
  wire \axi_rdata[2]_i_4_n_0 ;
  wire \axi_rdata[30]_i_2_n_0 ;
  wire \axi_rdata[31]_i_3_n_0 ;
  wire \axi_rdata[3]_i_2_n_0 ;
  wire \axi_rdata[3]_i_3_n_0 ;
  wire \axi_rdata[3]_i_4_n_0 ;
  wire \axi_rdata[4]_i_2_n_0 ;
  wire \axi_rdata[4]_i_3_n_0 ;
  wire \axi_rdata[4]_i_4_n_0 ;
  wire \axi_rdata[5]_i_2_n_0 ;
  wire \axi_rdata[5]_i_3_n_0 ;
  wire \axi_rdata[5]_i_4_n_0 ;
  wire \axi_rdata[6]_i_2_n_0 ;
  wire \axi_rdata[6]_i_3_n_0 ;
  wire \axi_rdata[6]_i_4_n_0 ;
  wire \axi_rdata[7]_i_2_n_0 ;
  wire \axi_rdata[7]_i_3_n_0 ;
  wire \axi_rdata[7]_i_4_n_0 ;
  wire \axi_rdata[7]_i_5_n_0 ;
  wire \axi_rdata[8]_i_2_n_0 ;
  wire axi_rvalid_i_1_n_0;
  wire axi_wready0;
  wire [31:2]data0;
  wire [7:0]dob;
  wire [16:0]i_addr;
  wire i_en;
  wire [7:0]i_px;
  wire i_wen;
  wire o_mf;
  wire [2:0]p_0_in;
  wire [31:7]p_1_in;
  wire [31:0]reg_data_out;
  wire s00_axi_aclk;
  wire [2:0]s00_axi_araddr;
  wire s00_axi_aresetn;
  wire s00_axi_arvalid;
  wire [2:0]s00_axi_awaddr;
  wire s00_axi_awvalid;
  wire s00_axi_bready;
  wire s00_axi_bvalid;
  wire [31:0]s00_axi_rdata;
  wire s00_axi_rready;
  wire s00_axi_rvalid;
  wire [31:0]s00_axi_wdata;
  wire [3:0]s00_axi_wstrb;
  wire s00_axi_wvalid;
  wire s_clk;
  wire [2:0]sel0;
  wire \slv_reg0_reg_n_0_[0] ;
  wire [16:0]slv_reg1;
  wire \slv_reg1[15]_i_1_n_0 ;
  wire \slv_reg1[23]_i_1_n_0 ;
  wire \slv_reg1[31]_i_1_n_0 ;
  wire \slv_reg1[7]_i_1_n_0 ;
  wire \slv_reg1_reg_n_0_[17] ;
  wire \slv_reg1_reg_n_0_[18] ;
  wire \slv_reg1_reg_n_0_[19] ;
  wire \slv_reg1_reg_n_0_[20] ;
  wire \slv_reg1_reg_n_0_[21] ;
  wire \slv_reg1_reg_n_0_[22] ;
  wire \slv_reg1_reg_n_0_[23] ;
  wire \slv_reg1_reg_n_0_[24] ;
  wire \slv_reg1_reg_n_0_[25] ;
  wire \slv_reg1_reg_n_0_[26] ;
  wire \slv_reg1_reg_n_0_[27] ;
  wire \slv_reg1_reg_n_0_[28] ;
  wire \slv_reg1_reg_n_0_[29] ;
  wire \slv_reg1_reg_n_0_[30] ;
  wire \slv_reg1_reg_n_0_[31] ;
  wire [7:0]slv_reg2;
  wire \slv_reg2[15]_i_1_n_0 ;
  wire \slv_reg2[23]_i_1_n_0 ;
  wire \slv_reg2[31]_i_1_n_0 ;
  wire \slv_reg2[7]_i_1_n_0 ;
  wire \slv_reg2_reg_n_0_[10] ;
  wire \slv_reg2_reg_n_0_[11] ;
  wire \slv_reg2_reg_n_0_[12] ;
  wire \slv_reg2_reg_n_0_[13] ;
  wire \slv_reg2_reg_n_0_[14] ;
  wire \slv_reg2_reg_n_0_[15] ;
  wire \slv_reg2_reg_n_0_[16] ;
  wire \slv_reg2_reg_n_0_[17] ;
  wire \slv_reg2_reg_n_0_[18] ;
  wire \slv_reg2_reg_n_0_[19] ;
  wire \slv_reg2_reg_n_0_[20] ;
  wire \slv_reg2_reg_n_0_[21] ;
  wire \slv_reg2_reg_n_0_[22] ;
  wire \slv_reg2_reg_n_0_[23] ;
  wire \slv_reg2_reg_n_0_[24] ;
  wire \slv_reg2_reg_n_0_[25] ;
  wire \slv_reg2_reg_n_0_[26] ;
  wire \slv_reg2_reg_n_0_[27] ;
  wire \slv_reg2_reg_n_0_[28] ;
  wire \slv_reg2_reg_n_0_[29] ;
  wire \slv_reg2_reg_n_0_[30] ;
  wire \slv_reg2_reg_n_0_[31] ;
  wire \slv_reg2_reg_n_0_[8] ;
  wire \slv_reg2_reg_n_0_[9] ;
  wire [31:0]slv_reg4;
  wire \slv_reg4[15]_i_1_n_0 ;
  wire \slv_reg4[23]_i_1_n_0 ;
  wire \slv_reg4[31]_i_1_n_0 ;
  wire \slv_reg4[7]_i_1_n_0 ;
  wire slv_reg_rden;
  wire slv_reg_wren__2;
  wire web;

  LUT6 #(
    .INIT(64'hBFFFBF00BF00BF00)) 
    aw_en_i_1
       (.I0(S_AXI_AWREADY),
        .I1(s00_axi_awvalid),
        .I2(s00_axi_wvalid),
        .I3(aw_en_reg_n_0),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(aw_en_i_1_n_0));
  FDSE aw_en_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(aw_en_i_1_n_0),
        .Q(aw_en_reg_n_0),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[0]),
        .Q(sel0[0]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[1]),
        .Q(sel0[1]),
        .S(axi_awready_i_1_n_0));
  FDSE \axi_araddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_arready0),
        .D(s00_axi_araddr[2]),
        .Q(sel0[2]),
        .S(axi_awready_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    axi_arready_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .O(axi_arready0));
  FDRE axi_arready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_arready0),
        .Q(S_AXI_ARREADY),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[2] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[0]),
        .Q(p_0_in[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[3] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[1]),
        .Q(p_0_in[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_awaddr_reg[4] 
       (.C(s00_axi_aclk),
        .CE(axi_awready0),
        .D(s00_axi_awaddr[2]),
        .Q(p_0_in[2]),
        .R(axi_awready_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    axi_awready_i_1
       (.I0(s00_axi_aresetn),
        .O(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h0080)) 
    axi_awready_i_2
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_AWREADY),
        .O(axi_awready0));
  FDRE axi_awready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_awready0),
        .Q(S_AXI_AWREADY),
        .R(axi_awready_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000FFFF80008000)) 
    axi_bvalid_i_1
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .I4(s00_axi_bready),
        .I5(s00_axi_bvalid),
        .O(axi_bvalid_i_1_n_0));
  FDRE axi_bvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_bvalid_i_1_n_0),
        .Q(s00_axi_bvalid),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[0]_i_2 
       (.I0(slv_reg4[0]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[0]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[0]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[0]),
        .I4(sel0[0]),
        .I5(dob[0]),
        .O(\axi_rdata[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[0]_i_4 
       (.I0(slv_reg1[0]),
        .I1(sel0[0]),
        .I2(\slv_reg0_reg_n_0_[0] ),
        .O(\axi_rdata[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[10]_i_1 
       (.I0(slv_reg1[10]),
        .I1(\slv_reg2_reg_n_0_[10] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[10]),
        .I5(sel0[0]),
        .O(reg_data_out[10]));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[11]_i_1 
       (.I0(slv_reg1[11]),
        .I1(\slv_reg2_reg_n_0_[11] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[11]),
        .I5(sel0[0]),
        .O(reg_data_out[11]));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[12]_i_1 
       (.I0(slv_reg1[12]),
        .I1(\slv_reg2_reg_n_0_[12] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[12]),
        .I5(sel0[0]),
        .O(reg_data_out[12]));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[13]_i_1 
       (.I0(slv_reg1[13]),
        .I1(\slv_reg2_reg_n_0_[13] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[13]),
        .I5(sel0[0]),
        .O(reg_data_out[13]));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[14]_i_1 
       (.I0(slv_reg1[14]),
        .I1(\slv_reg2_reg_n_0_[14] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[14]),
        .I5(sel0[0]),
        .O(reg_data_out[14]));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[15]_i_1 
       (.I0(slv_reg1[15]),
        .I1(\slv_reg2_reg_n_0_[15] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[15]),
        .I5(sel0[0]),
        .O(reg_data_out[15]));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[16]_i_1 
       (.I0(\axi_rdata[16]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[16]),
        .I4(sel0[0]),
        .O(reg_data_out[16]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[16]_i_2 
       (.I0(sel0[2]),
        .I1(data0[16]),
        .I2(sel0[0]),
        .I3(slv_reg1[16]),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[16] ),
        .O(\axi_rdata[16]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[17]_i_1 
       (.I0(\axi_rdata[17]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[17]),
        .I4(sel0[0]),
        .O(reg_data_out[17]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[17]_i_2 
       (.I0(sel0[2]),
        .I1(data0[17]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[17] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[17] ),
        .O(\axi_rdata[17]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[18]_i_1 
       (.I0(\axi_rdata[18]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[18]),
        .I4(sel0[0]),
        .O(reg_data_out[18]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[18]_i_2 
       (.I0(sel0[2]),
        .I1(data0[18]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[18] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[18] ),
        .O(\axi_rdata[18]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[19]_i_1 
       (.I0(\axi_rdata[19]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[19]),
        .I4(sel0[0]),
        .O(reg_data_out[19]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[19]_i_2 
       (.I0(sel0[2]),
        .I1(data0[19]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[19] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[19] ),
        .O(\axi_rdata[19]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[1]_i_2 
       (.I0(slv_reg4[1]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[1]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[1]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[1]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[1]),
        .I4(sel0[0]),
        .I5(dob[1]),
        .O(\axi_rdata[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[1]_i_4 
       (.I0(slv_reg1[1]),
        .I1(sel0[0]),
        .I2(web),
        .O(\axi_rdata[1]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[20]_i_1 
       (.I0(\axi_rdata[20]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[20]),
        .I4(sel0[0]),
        .O(reg_data_out[20]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[20]_i_2 
       (.I0(sel0[2]),
        .I1(data0[20]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[20] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[20] ),
        .O(\axi_rdata[20]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[21]_i_1 
       (.I0(\axi_rdata[21]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[21]),
        .I4(sel0[0]),
        .O(reg_data_out[21]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[21]_i_2 
       (.I0(sel0[2]),
        .I1(data0[21]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[21] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[21] ),
        .O(\axi_rdata[21]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[22]_i_1 
       (.I0(\axi_rdata[22]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[22]),
        .I4(sel0[0]),
        .O(reg_data_out[22]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[22]_i_2 
       (.I0(sel0[2]),
        .I1(data0[22]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[22] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[22] ),
        .O(\axi_rdata[22]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[23]_i_1 
       (.I0(\axi_rdata[23]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[23]),
        .I4(sel0[0]),
        .O(reg_data_out[23]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[23]_i_2 
       (.I0(sel0[2]),
        .I1(data0[23]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[23] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[23] ),
        .O(\axi_rdata[23]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[24]_i_1 
       (.I0(\axi_rdata[24]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[24]),
        .I4(sel0[0]),
        .O(reg_data_out[24]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[24]_i_2 
       (.I0(sel0[2]),
        .I1(data0[24]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[24] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[24] ),
        .O(\axi_rdata[24]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[25]_i_1 
       (.I0(\axi_rdata[25]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[25]),
        .I4(sel0[0]),
        .O(reg_data_out[25]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[25]_i_2 
       (.I0(sel0[2]),
        .I1(data0[25]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[25] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[25] ),
        .O(\axi_rdata[25]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[26]_i_1 
       (.I0(\axi_rdata[26]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[26]),
        .I4(sel0[0]),
        .O(reg_data_out[26]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[26]_i_2 
       (.I0(sel0[2]),
        .I1(data0[26]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[26] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[26] ),
        .O(\axi_rdata[26]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[27]_i_1 
       (.I0(\axi_rdata[27]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[27]),
        .I4(sel0[0]),
        .O(reg_data_out[27]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[27]_i_2 
       (.I0(sel0[2]),
        .I1(data0[27]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[27] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[27] ),
        .O(\axi_rdata[27]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[28]_i_1 
       (.I0(\axi_rdata[28]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[28]),
        .I4(sel0[0]),
        .O(reg_data_out[28]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[28]_i_2 
       (.I0(sel0[2]),
        .I1(data0[28]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[28] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[28] ),
        .O(\axi_rdata[28]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[29]_i_1 
       (.I0(\axi_rdata[29]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[29]),
        .I4(sel0[0]),
        .O(reg_data_out[29]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[29]_i_2 
       (.I0(sel0[2]),
        .I1(data0[29]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[29] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[29] ),
        .O(\axi_rdata[29]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[2]_i_2 
       (.I0(slv_reg4[2]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[2]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[2]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[2]),
        .I4(sel0[0]),
        .I5(dob[2]),
        .O(\axi_rdata[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[2]_i_4 
       (.I0(slv_reg1[2]),
        .I1(sel0[0]),
        .I2(data0[2]),
        .O(\axi_rdata[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[30]_i_1 
       (.I0(\axi_rdata[30]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[30]),
        .I4(sel0[0]),
        .O(reg_data_out[30]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[30]_i_2 
       (.I0(sel0[2]),
        .I1(data0[30]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[30] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[30] ),
        .O(\axi_rdata[30]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h08)) 
    \axi_rdata[31]_i_1 
       (.I0(S_AXI_ARREADY),
        .I1(s00_axi_arvalid),
        .I2(s00_axi_rvalid),
        .O(slv_reg_rden));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[31]_i_2 
       (.I0(\axi_rdata[31]_i_3_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[31]),
        .I4(sel0[0]),
        .O(reg_data_out[31]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[31]_i_3 
       (.I0(sel0[2]),
        .I1(data0[31]),
        .I2(sel0[0]),
        .I3(\slv_reg1_reg_n_0_[31] ),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[31] ),
        .O(\axi_rdata[31]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[3]_i_2 
       (.I0(slv_reg4[3]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[3]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[3]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[3]),
        .I4(sel0[0]),
        .I5(dob[3]),
        .O(\axi_rdata[3]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[3]_i_4 
       (.I0(slv_reg1[3]),
        .I1(sel0[0]),
        .I2(data0[3]),
        .O(\axi_rdata[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[4]_i_2 
       (.I0(slv_reg4[4]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[4]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[4]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[4]),
        .I4(sel0[0]),
        .I5(dob[4]),
        .O(\axi_rdata[4]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[4]_i_4 
       (.I0(slv_reg1[4]),
        .I1(sel0[0]),
        .I2(data0[4]),
        .O(\axi_rdata[4]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[5]_i_2 
       (.I0(slv_reg4[5]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[5]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[5]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[5]),
        .I4(sel0[0]),
        .I5(dob[5]),
        .O(\axi_rdata[5]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[5]_i_4 
       (.I0(slv_reg1[5]),
        .I1(sel0[0]),
        .I2(data0[5]),
        .O(\axi_rdata[5]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[6]_i_2 
       (.I0(slv_reg4[6]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[6]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[6]_i_3 
       (.I0(sel0[2]),
        .I1(\axi_rdata[6]_i_4_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[6]),
        .I4(sel0[0]),
        .I5(dob[6]),
        .O(\axi_rdata[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[6]_i_4 
       (.I0(slv_reg1[6]),
        .I1(sel0[0]),
        .I2(data0[6]),
        .O(\axi_rdata[6]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \axi_rdata[7]_i_2 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .O(\axi_rdata[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h8A00)) 
    \axi_rdata[7]_i_3 
       (.I0(slv_reg4[7]),
        .I1(sel0[1]),
        .I2(sel0[0]),
        .I3(sel0[2]),
        .O(\axi_rdata[7]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h5C5C54040C0C5404)) 
    \axi_rdata[7]_i_4 
       (.I0(sel0[2]),
        .I1(\axi_rdata[7]_i_5_n_0 ),
        .I2(sel0[1]),
        .I3(slv_reg2[7]),
        .I4(sel0[0]),
        .I5(dob[7]),
        .O(\axi_rdata[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \axi_rdata[7]_i_5 
       (.I0(slv_reg1[7]),
        .I1(sel0[0]),
        .I2(data0[7]),
        .O(\axi_rdata[7]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'h8A8ABA8A)) 
    \axi_rdata[8]_i_1 
       (.I0(\axi_rdata[8]_i_2_n_0 ),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(slv_reg4[8]),
        .I4(sel0[0]),
        .O(reg_data_out[8]));
  LUT6 #(
    .INIT(64'h0505F4040000F404)) 
    \axi_rdata[8]_i_2 
       (.I0(sel0[2]),
        .I1(data0[8]),
        .I2(sel0[0]),
        .I3(slv_reg1[8]),
        .I4(sel0[1]),
        .I5(\slv_reg2_reg_n_0_[8] ),
        .O(\axi_rdata[8]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000A000A0FC000C0)) 
    \axi_rdata[9]_i_1 
       (.I0(slv_reg1[9]),
        .I1(\slv_reg2_reg_n_0_[9] ),
        .I2(sel0[1]),
        .I3(sel0[2]),
        .I4(slv_reg4[9]),
        .I5(sel0[0]),
        .O(reg_data_out[9]));
  FDRE \axi_rdata_reg[0] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[0]),
        .Q(s00_axi_rdata[0]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[0]_i_1 
       (.I0(\axi_rdata[0]_i_2_n_0 ),
        .I1(\axi_rdata[0]_i_3_n_0 ),
        .O(reg_data_out[0]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[10] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[10]),
        .Q(s00_axi_rdata[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[11] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[11]),
        .Q(s00_axi_rdata[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[12] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[12]),
        .Q(s00_axi_rdata[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[13] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[13]),
        .Q(s00_axi_rdata[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[14] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[14]),
        .Q(s00_axi_rdata[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[15] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[15]),
        .Q(s00_axi_rdata[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[16] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[16]),
        .Q(s00_axi_rdata[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[17] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[17]),
        .Q(s00_axi_rdata[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[18] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[18]),
        .Q(s00_axi_rdata[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[19] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[19]),
        .Q(s00_axi_rdata[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[1] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[1]),
        .Q(s00_axi_rdata[1]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[1]_i_1 
       (.I0(\axi_rdata[1]_i_2_n_0 ),
        .I1(\axi_rdata[1]_i_3_n_0 ),
        .O(reg_data_out[1]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[20] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[20]),
        .Q(s00_axi_rdata[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[21] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[21]),
        .Q(s00_axi_rdata[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[22] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[22]),
        .Q(s00_axi_rdata[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[23] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[23]),
        .Q(s00_axi_rdata[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[24] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[24]),
        .Q(s00_axi_rdata[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[25] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[25]),
        .Q(s00_axi_rdata[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[26] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[26]),
        .Q(s00_axi_rdata[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[27] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[27]),
        .Q(s00_axi_rdata[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[28] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[28]),
        .Q(s00_axi_rdata[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[29] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[29]),
        .Q(s00_axi_rdata[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[2] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[2]),
        .Q(s00_axi_rdata[2]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[2]_i_1 
       (.I0(\axi_rdata[2]_i_2_n_0 ),
        .I1(\axi_rdata[2]_i_3_n_0 ),
        .O(reg_data_out[2]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[30] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[30]),
        .Q(s00_axi_rdata[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[31] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[31]),
        .Q(s00_axi_rdata[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[3] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[3]),
        .Q(s00_axi_rdata[3]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[3]_i_1 
       (.I0(\axi_rdata[3]_i_2_n_0 ),
        .I1(\axi_rdata[3]_i_3_n_0 ),
        .O(reg_data_out[3]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[4] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[4]),
        .Q(s00_axi_rdata[4]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[4]_i_1 
       (.I0(\axi_rdata[4]_i_2_n_0 ),
        .I1(\axi_rdata[4]_i_3_n_0 ),
        .O(reg_data_out[4]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[5] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[5]),
        .Q(s00_axi_rdata[5]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[5]_i_1 
       (.I0(\axi_rdata[5]_i_2_n_0 ),
        .I1(\axi_rdata[5]_i_3_n_0 ),
        .O(reg_data_out[5]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[6] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[6]),
        .Q(s00_axi_rdata[6]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[6]_i_1 
       (.I0(\axi_rdata[6]_i_2_n_0 ),
        .I1(\axi_rdata[6]_i_3_n_0 ),
        .O(reg_data_out[6]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[7] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[7]),
        .Q(s00_axi_rdata[7]),
        .R(axi_awready_i_1_n_0));
  MUXF7 \axi_rdata_reg[7]_i_1 
       (.I0(\axi_rdata[7]_i_3_n_0 ),
        .I1(\axi_rdata[7]_i_4_n_0 ),
        .O(reg_data_out[7]),
        .S(\axi_rdata[7]_i_2_n_0 ));
  FDRE \axi_rdata_reg[8] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[8]),
        .Q(s00_axi_rdata[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \axi_rdata_reg[9] 
       (.C(s00_axi_aclk),
        .CE(slv_reg_rden),
        .D(reg_data_out[9]),
        .Q(s00_axi_rdata[9]),
        .R(axi_awready_i_1_n_0));
  LUT4 #(
    .INIT(16'h08F8)) 
    axi_rvalid_i_1
       (.I0(s00_axi_arvalid),
        .I1(S_AXI_ARREADY),
        .I2(s00_axi_rvalid),
        .I3(s00_axi_rready),
        .O(axi_rvalid_i_1_n_0));
  FDRE axi_rvalid_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_rvalid_i_1_n_0),
        .Q(s00_axi_rvalid),
        .R(axi_awready_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    axi_wready_i_1
       (.I0(aw_en_reg_n_0),
        .I1(s00_axi_wvalid),
        .I2(s00_axi_awvalid),
        .I3(S_AXI_WREADY),
        .O(axi_wready0));
  FDRE axi_wready_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(axi_wready0),
        .Q(S_AXI_WREADY),
        .R(axi_awready_i_1_n_0));
  OV7670_QVGA_Image_Visualization_0_1_rams_tdp_rf_rf bram_inst
       (.Q({web,\slv_reg0_reg_n_0_[0] }),
        .RAM_reg_bram_22_0(slv_reg2),
        .RAM_reg_mux_sel_reg_3_0(slv_reg1),
        .data0(data0[8]),
        .dob(dob),
        .i_addr(i_addr),
        .i_en(i_en),
        .i_px(i_px),
        .i_wen(i_wen),
        .o_mf(o_mf),
        .s00_axi_aclk(s00_axi_aclk),
        .s_clk(s_clk));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(p_1_in[23]));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(p_1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \slv_reg0[31]_i_2 
       (.I0(s00_axi_awvalid),
        .I1(S_AXI_AWREADY),
        .I2(S_AXI_WREADY),
        .I3(s00_axi_wvalid),
        .O(slv_reg_wren__2));
  LUT5 #(
    .INIT(32'h00020000)) 
    \slv_reg0[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(p_1_in[7]));
  FDRE \slv_reg0_reg[0] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[0]),
        .Q(\slv_reg0_reg_n_0_[0] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[16] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[16]),
        .Q(data0[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[17] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[17]),
        .Q(data0[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[18] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[18]),
        .Q(data0[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[19] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[19]),
        .Q(data0[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[1] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[1]),
        .Q(web),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[20] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[20]),
        .Q(data0[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[21] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[21]),
        .Q(data0[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[22] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[22]),
        .Q(data0[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[23] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[23]),
        .D(s00_axi_wdata[23]),
        .Q(data0[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[24] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[24]),
        .Q(data0[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[25] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[25]),
        .Q(data0[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[26] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[26]),
        .Q(data0[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[27] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[27]),
        .Q(data0[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[28] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[28]),
        .Q(data0[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[29] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[29]),
        .Q(data0[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[2] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[2]),
        .Q(data0[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[30] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[30]),
        .Q(data0[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[31] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[31]),
        .D(s00_axi_wdata[31]),
        .Q(data0[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[3] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[3]),
        .Q(data0[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[4] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[4]),
        .Q(data0[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[5] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[5]),
        .Q(data0[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[6] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[6]),
        .Q(data0[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg0_reg[7] 
       (.C(s00_axi_aclk),
        .CE(p_1_in[7]),
        .D(s00_axi_wdata[7]),
        .Q(data0[7]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg1[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[1]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[0]),
        .O(\slv_reg1[7]_i_1_n_0 ));
  FDRE \slv_reg1_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg1[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg1[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg1[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg1[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg1[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg1[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg1[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg1[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg1_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg1_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg1_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg1[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg1_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg1_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg1_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg1_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg1_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg1_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg1_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg1_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg1_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg1_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg1[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg1_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg1_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg1[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg1[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg1[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg1[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg1[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg1[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg1_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg1[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg1[9]),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[1]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[2]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[3]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg2[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[2]),
        .I2(p_0_in[0]),
        .I3(s00_axi_wstrb[0]),
        .I4(p_0_in[1]),
        .O(\slv_reg2[7]_i_1_n_0 ));
  FDRE \slv_reg2_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg2[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(\slv_reg2_reg_n_0_[10] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(\slv_reg2_reg_n_0_[11] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(\slv_reg2_reg_n_0_[12] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(\slv_reg2_reg_n_0_[13] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(\slv_reg2_reg_n_0_[14] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(\slv_reg2_reg_n_0_[15] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(\slv_reg2_reg_n_0_[16] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(\slv_reg2_reg_n_0_[17] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(\slv_reg2_reg_n_0_[18] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(\slv_reg2_reg_n_0_[19] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg2[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(\slv_reg2_reg_n_0_[20] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(\slv_reg2_reg_n_0_[21] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(\slv_reg2_reg_n_0_[22] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(\slv_reg2_reg_n_0_[23] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(\slv_reg2_reg_n_0_[24] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(\slv_reg2_reg_n_0_[25] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(\slv_reg2_reg_n_0_[26] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(\slv_reg2_reg_n_0_[27] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(\slv_reg2_reg_n_0_[28] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(\slv_reg2_reg_n_0_[29] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg2[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(\slv_reg2_reg_n_0_[30] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(\slv_reg2_reg_n_0_[31] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg2[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg2[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg2[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg2[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg2[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(\slv_reg2_reg_n_0_[8] ),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg2_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg2[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(\slv_reg2_reg_n_0_[9] ),
        .R(axi_awready_i_1_n_0));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[15]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[1]),
        .O(\slv_reg4[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[23]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[2]),
        .O(\slv_reg4[23]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[31]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[3]),
        .O(\slv_reg4[31]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h02000000)) 
    \slv_reg4[7]_i_1 
       (.I0(slv_reg_wren__2),
        .I1(p_0_in[1]),
        .I2(p_0_in[0]),
        .I3(p_0_in[2]),
        .I4(s00_axi_wstrb[0]),
        .O(\slv_reg4[7]_i_1_n_0 ));
  FDRE \slv_reg4_reg[0] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[0]),
        .Q(slv_reg4[0]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[10] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[10]),
        .Q(slv_reg4[10]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[11] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[11]),
        .Q(slv_reg4[11]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[12] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[12]),
        .Q(slv_reg4[12]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[13] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[13]),
        .Q(slv_reg4[13]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[14] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[14]),
        .Q(slv_reg4[14]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[15] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[15]),
        .Q(slv_reg4[15]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[16] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[16]),
        .Q(slv_reg4[16]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[17] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[17]),
        .Q(slv_reg4[17]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[18] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[18]),
        .Q(slv_reg4[18]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[19] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[19]),
        .Q(slv_reg4[19]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[1] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[1]),
        .Q(slv_reg4[1]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[20] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[20]),
        .Q(slv_reg4[20]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[21] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[21]),
        .Q(slv_reg4[21]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[22] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[22]),
        .Q(slv_reg4[22]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[23] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[23]_i_1_n_0 ),
        .D(s00_axi_wdata[23]),
        .Q(slv_reg4[23]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[24] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[24]),
        .Q(slv_reg4[24]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[25] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[25]),
        .Q(slv_reg4[25]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[26] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[26]),
        .Q(slv_reg4[26]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[27] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[27]),
        .Q(slv_reg4[27]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[28] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[28]),
        .Q(slv_reg4[28]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[29] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[29]),
        .Q(slv_reg4[29]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[2] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[2]),
        .Q(slv_reg4[2]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[30] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[30]),
        .Q(slv_reg4[30]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[31] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[31]_i_1_n_0 ),
        .D(s00_axi_wdata[31]),
        .Q(slv_reg4[31]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[3] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[3]),
        .Q(slv_reg4[3]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[4] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[4]),
        .Q(slv_reg4[4]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[5] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[5]),
        .Q(slv_reg4[5]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[6] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[6]),
        .Q(slv_reg4[6]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[7] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[7]_i_1_n_0 ),
        .D(s00_axi_wdata[7]),
        .Q(slv_reg4[7]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[8] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[8]),
        .Q(slv_reg4[8]),
        .R(axi_awready_i_1_n_0));
  FDRE \slv_reg4_reg[9] 
       (.C(s00_axi_aclk),
        .CE(\slv_reg4[15]_i_1_n_0 ),
        .D(s00_axi_wdata[9]),
        .Q(slv_reg4[9]),
        .R(axi_awready_i_1_n_0));
endmodule

(* ORIG_REF_NAME = "rams_tdp_rf_rf" *) 
module OV7670_QVGA_Image_Visualization_0_1_rams_tdp_rf_rf
   (o_mf,
    data0,
    dob,
    i_en,
    s_clk,
    Q,
    s00_axi_aclk,
    i_addr,
    RAM_reg_mux_sel_reg_3_0,
    i_px,
    RAM_reg_bram_22_0,
    i_wen);
  output o_mf;
  output [0:0]data0;
  output [7:0]dob;
  input i_en;
  input s_clk;
  input [1:0]Q;
  input s00_axi_aclk;
  input [16:0]i_addr;
  input [16:0]RAM_reg_mux_sel_reg_3_0;
  input [7:0]i_px;
  input [7:0]RAM_reg_bram_22_0;
  input i_wen;

  wire [1:0]Q;
  wire RAM_reg_bram_10_i_1_n_0;
  wire RAM_reg_bram_10_i_2_n_0;
  wire RAM_reg_bram_10_i_3_n_0;
  wire RAM_reg_bram_10_i_4_n_0;
  wire RAM_reg_bram_10_i_5_n_0;
  wire RAM_reg_bram_10_i_6_n_0;
  wire RAM_reg_bram_10_n_0;
  wire RAM_reg_bram_10_n_1;
  wire RAM_reg_bram_10_n_132;
  wire RAM_reg_bram_10_n_133;
  wire RAM_reg_bram_10_n_134;
  wire RAM_reg_bram_10_n_135;
  wire RAM_reg_bram_10_n_136;
  wire RAM_reg_bram_10_n_137;
  wire RAM_reg_bram_10_n_138;
  wire RAM_reg_bram_10_n_139;
  wire RAM_reg_bram_10_n_28;
  wire RAM_reg_bram_10_n_29;
  wire RAM_reg_bram_10_n_30;
  wire RAM_reg_bram_10_n_31;
  wire RAM_reg_bram_10_n_32;
  wire RAM_reg_bram_10_n_33;
  wire RAM_reg_bram_10_n_34;
  wire RAM_reg_bram_10_n_35;
  wire RAM_reg_bram_10_n_60;
  wire RAM_reg_bram_10_n_61;
  wire RAM_reg_bram_10_n_62;
  wire RAM_reg_bram_10_n_63;
  wire RAM_reg_bram_10_n_64;
  wire RAM_reg_bram_10_n_65;
  wire RAM_reg_bram_10_n_66;
  wire RAM_reg_bram_10_n_67;
  wire RAM_reg_bram_11_i_1_n_0;
  wire RAM_reg_bram_11_i_2_n_0;
  wire RAM_reg_bram_11_i_3_n_0;
  wire RAM_reg_bram_11_i_4_n_0;
  wire RAM_reg_bram_11_i_5_n_0;
  wire RAM_reg_bram_11_i_6_n_0;
  wire RAM_reg_bram_11_n_124;
  wire RAM_reg_bram_11_n_125;
  wire RAM_reg_bram_11_n_126;
  wire RAM_reg_bram_11_n_127;
  wire RAM_reg_bram_11_n_128;
  wire RAM_reg_bram_11_n_129;
  wire RAM_reg_bram_11_n_130;
  wire RAM_reg_bram_11_n_131;
  wire RAM_reg_bram_11_n_92;
  wire RAM_reg_bram_11_n_93;
  wire RAM_reg_bram_11_n_94;
  wire RAM_reg_bram_11_n_95;
  wire RAM_reg_bram_11_n_96;
  wire RAM_reg_bram_11_n_97;
  wire RAM_reg_bram_11_n_98;
  wire RAM_reg_bram_11_n_99;
  wire RAM_reg_bram_12_i_1_n_0;
  wire RAM_reg_bram_12_i_2_n_0;
  wire RAM_reg_bram_12_i_3_n_0;
  wire RAM_reg_bram_12_i_4_n_0;
  wire RAM_reg_bram_12_n_0;
  wire RAM_reg_bram_12_n_1;
  wire RAM_reg_bram_12_n_132;
  wire RAM_reg_bram_12_n_133;
  wire RAM_reg_bram_12_n_134;
  wire RAM_reg_bram_12_n_135;
  wire RAM_reg_bram_12_n_136;
  wire RAM_reg_bram_12_n_137;
  wire RAM_reg_bram_12_n_138;
  wire RAM_reg_bram_12_n_139;
  wire RAM_reg_bram_12_n_28;
  wire RAM_reg_bram_12_n_29;
  wire RAM_reg_bram_12_n_30;
  wire RAM_reg_bram_12_n_31;
  wire RAM_reg_bram_12_n_32;
  wire RAM_reg_bram_12_n_33;
  wire RAM_reg_bram_12_n_34;
  wire RAM_reg_bram_12_n_35;
  wire RAM_reg_bram_12_n_60;
  wire RAM_reg_bram_12_n_61;
  wire RAM_reg_bram_12_n_62;
  wire RAM_reg_bram_12_n_63;
  wire RAM_reg_bram_12_n_64;
  wire RAM_reg_bram_12_n_65;
  wire RAM_reg_bram_12_n_66;
  wire RAM_reg_bram_12_n_67;
  wire RAM_reg_bram_13_i_1_n_0;
  wire RAM_reg_bram_13_i_2_n_0;
  wire RAM_reg_bram_13_i_3_n_0;
  wire RAM_reg_bram_13_i_4_n_0;
  wire RAM_reg_bram_13_n_0;
  wire RAM_reg_bram_13_n_1;
  wire RAM_reg_bram_13_n_132;
  wire RAM_reg_bram_13_n_133;
  wire RAM_reg_bram_13_n_134;
  wire RAM_reg_bram_13_n_135;
  wire RAM_reg_bram_13_n_136;
  wire RAM_reg_bram_13_n_137;
  wire RAM_reg_bram_13_n_138;
  wire RAM_reg_bram_13_n_139;
  wire RAM_reg_bram_13_n_28;
  wire RAM_reg_bram_13_n_29;
  wire RAM_reg_bram_13_n_30;
  wire RAM_reg_bram_13_n_31;
  wire RAM_reg_bram_13_n_32;
  wire RAM_reg_bram_13_n_33;
  wire RAM_reg_bram_13_n_34;
  wire RAM_reg_bram_13_n_35;
  wire RAM_reg_bram_13_n_60;
  wire RAM_reg_bram_13_n_61;
  wire RAM_reg_bram_13_n_62;
  wire RAM_reg_bram_13_n_63;
  wire RAM_reg_bram_13_n_64;
  wire RAM_reg_bram_13_n_65;
  wire RAM_reg_bram_13_n_66;
  wire RAM_reg_bram_13_n_67;
  wire RAM_reg_bram_14_i_1_n_0;
  wire RAM_reg_bram_14_i_2_n_0;
  wire RAM_reg_bram_14_i_3_n_0;
  wire RAM_reg_bram_14_i_4_n_0;
  wire RAM_reg_bram_14_n_0;
  wire RAM_reg_bram_14_n_1;
  wire RAM_reg_bram_14_n_132;
  wire RAM_reg_bram_14_n_133;
  wire RAM_reg_bram_14_n_134;
  wire RAM_reg_bram_14_n_135;
  wire RAM_reg_bram_14_n_136;
  wire RAM_reg_bram_14_n_137;
  wire RAM_reg_bram_14_n_138;
  wire RAM_reg_bram_14_n_139;
  wire RAM_reg_bram_14_n_28;
  wire RAM_reg_bram_14_n_29;
  wire RAM_reg_bram_14_n_30;
  wire RAM_reg_bram_14_n_31;
  wire RAM_reg_bram_14_n_32;
  wire RAM_reg_bram_14_n_33;
  wire RAM_reg_bram_14_n_34;
  wire RAM_reg_bram_14_n_35;
  wire RAM_reg_bram_14_n_60;
  wire RAM_reg_bram_14_n_61;
  wire RAM_reg_bram_14_n_62;
  wire RAM_reg_bram_14_n_63;
  wire RAM_reg_bram_14_n_64;
  wire RAM_reg_bram_14_n_65;
  wire RAM_reg_bram_14_n_66;
  wire RAM_reg_bram_14_n_67;
  wire RAM_reg_bram_15_i_1_n_0;
  wire RAM_reg_bram_15_i_2_n_0;
  wire RAM_reg_bram_15_i_3_n_0;
  wire RAM_reg_bram_15_i_4_n_0;
  wire RAM_reg_bram_15_n_0;
  wire RAM_reg_bram_15_n_1;
  wire RAM_reg_bram_15_n_132;
  wire RAM_reg_bram_15_n_133;
  wire RAM_reg_bram_15_n_134;
  wire RAM_reg_bram_15_n_135;
  wire RAM_reg_bram_15_n_136;
  wire RAM_reg_bram_15_n_137;
  wire RAM_reg_bram_15_n_138;
  wire RAM_reg_bram_15_n_139;
  wire RAM_reg_bram_15_n_28;
  wire RAM_reg_bram_15_n_29;
  wire RAM_reg_bram_15_n_30;
  wire RAM_reg_bram_15_n_31;
  wire RAM_reg_bram_15_n_32;
  wire RAM_reg_bram_15_n_33;
  wire RAM_reg_bram_15_n_34;
  wire RAM_reg_bram_15_n_35;
  wire RAM_reg_bram_15_n_60;
  wire RAM_reg_bram_15_n_61;
  wire RAM_reg_bram_15_n_62;
  wire RAM_reg_bram_15_n_63;
  wire RAM_reg_bram_15_n_64;
  wire RAM_reg_bram_15_n_65;
  wire RAM_reg_bram_15_n_66;
  wire RAM_reg_bram_15_n_67;
  wire RAM_reg_bram_16_i_1_n_0;
  wire RAM_reg_bram_16_i_2_n_0;
  wire RAM_reg_bram_16_i_3_n_0;
  wire RAM_reg_bram_16_i_4_n_0;
  wire RAM_reg_bram_16_n_0;
  wire RAM_reg_bram_16_n_1;
  wire RAM_reg_bram_16_n_132;
  wire RAM_reg_bram_16_n_133;
  wire RAM_reg_bram_16_n_134;
  wire RAM_reg_bram_16_n_135;
  wire RAM_reg_bram_16_n_136;
  wire RAM_reg_bram_16_n_137;
  wire RAM_reg_bram_16_n_138;
  wire RAM_reg_bram_16_n_139;
  wire RAM_reg_bram_16_n_28;
  wire RAM_reg_bram_16_n_29;
  wire RAM_reg_bram_16_n_30;
  wire RAM_reg_bram_16_n_31;
  wire RAM_reg_bram_16_n_32;
  wire RAM_reg_bram_16_n_33;
  wire RAM_reg_bram_16_n_34;
  wire RAM_reg_bram_16_n_35;
  wire RAM_reg_bram_16_n_60;
  wire RAM_reg_bram_16_n_61;
  wire RAM_reg_bram_16_n_62;
  wire RAM_reg_bram_16_n_63;
  wire RAM_reg_bram_16_n_64;
  wire RAM_reg_bram_16_n_65;
  wire RAM_reg_bram_16_n_66;
  wire RAM_reg_bram_16_n_67;
  wire RAM_reg_bram_17_i_1_n_0;
  wire RAM_reg_bram_17_i_2_n_0;
  wire RAM_reg_bram_17_i_3_n_0;
  wire RAM_reg_bram_17_i_4_n_0;
  wire RAM_reg_bram_17_n_0;
  wire RAM_reg_bram_17_n_1;
  wire RAM_reg_bram_17_n_132;
  wire RAM_reg_bram_17_n_133;
  wire RAM_reg_bram_17_n_134;
  wire RAM_reg_bram_17_n_135;
  wire RAM_reg_bram_17_n_136;
  wire RAM_reg_bram_17_n_137;
  wire RAM_reg_bram_17_n_138;
  wire RAM_reg_bram_17_n_139;
  wire RAM_reg_bram_17_n_28;
  wire RAM_reg_bram_17_n_29;
  wire RAM_reg_bram_17_n_30;
  wire RAM_reg_bram_17_n_31;
  wire RAM_reg_bram_17_n_32;
  wire RAM_reg_bram_17_n_33;
  wire RAM_reg_bram_17_n_34;
  wire RAM_reg_bram_17_n_35;
  wire RAM_reg_bram_17_n_60;
  wire RAM_reg_bram_17_n_61;
  wire RAM_reg_bram_17_n_62;
  wire RAM_reg_bram_17_n_63;
  wire RAM_reg_bram_17_n_64;
  wire RAM_reg_bram_17_n_65;
  wire RAM_reg_bram_17_n_66;
  wire RAM_reg_bram_17_n_67;
  wire RAM_reg_bram_18_i_1_n_0;
  wire RAM_reg_bram_18_i_2_n_0;
  wire RAM_reg_bram_18_i_3_n_0;
  wire RAM_reg_bram_18_i_4_n_0;
  wire RAM_reg_bram_18_n_0;
  wire RAM_reg_bram_18_n_1;
  wire RAM_reg_bram_18_n_132;
  wire RAM_reg_bram_18_n_133;
  wire RAM_reg_bram_18_n_134;
  wire RAM_reg_bram_18_n_135;
  wire RAM_reg_bram_18_n_136;
  wire RAM_reg_bram_18_n_137;
  wire RAM_reg_bram_18_n_138;
  wire RAM_reg_bram_18_n_139;
  wire RAM_reg_bram_18_n_28;
  wire RAM_reg_bram_18_n_29;
  wire RAM_reg_bram_18_n_30;
  wire RAM_reg_bram_18_n_31;
  wire RAM_reg_bram_18_n_32;
  wire RAM_reg_bram_18_n_33;
  wire RAM_reg_bram_18_n_34;
  wire RAM_reg_bram_18_n_35;
  wire RAM_reg_bram_18_n_60;
  wire RAM_reg_bram_18_n_61;
  wire RAM_reg_bram_18_n_62;
  wire RAM_reg_bram_18_n_63;
  wire RAM_reg_bram_18_n_64;
  wire RAM_reg_bram_18_n_65;
  wire RAM_reg_bram_18_n_66;
  wire RAM_reg_bram_18_n_67;
  wire RAM_reg_bram_19_i_1_n_0;
  wire RAM_reg_bram_19_i_2_n_0;
  wire RAM_reg_bram_19_i_3_n_0;
  wire RAM_reg_bram_19_i_4_n_0;
  wire RAM_reg_bram_19_n_124;
  wire RAM_reg_bram_19_n_125;
  wire RAM_reg_bram_19_n_126;
  wire RAM_reg_bram_19_n_127;
  wire RAM_reg_bram_19_n_128;
  wire RAM_reg_bram_19_n_129;
  wire RAM_reg_bram_19_n_130;
  wire RAM_reg_bram_19_n_131;
  wire RAM_reg_bram_19_n_92;
  wire RAM_reg_bram_19_n_93;
  wire RAM_reg_bram_19_n_94;
  wire RAM_reg_bram_19_n_95;
  wire RAM_reg_bram_19_n_96;
  wire RAM_reg_bram_19_n_97;
  wire RAM_reg_bram_19_n_98;
  wire RAM_reg_bram_19_n_99;
  wire RAM_reg_bram_20_i_1_n_0;
  wire RAM_reg_bram_20_i_2_n_0;
  wire RAM_reg_bram_20_i_3_n_0;
  wire RAM_reg_bram_20_i_4_n_0;
  wire RAM_reg_bram_20_n_0;
  wire RAM_reg_bram_20_n_1;
  wire RAM_reg_bram_20_n_132;
  wire RAM_reg_bram_20_n_133;
  wire RAM_reg_bram_20_n_134;
  wire RAM_reg_bram_20_n_135;
  wire RAM_reg_bram_20_n_136;
  wire RAM_reg_bram_20_n_137;
  wire RAM_reg_bram_20_n_138;
  wire RAM_reg_bram_20_n_139;
  wire RAM_reg_bram_20_n_28;
  wire RAM_reg_bram_20_n_29;
  wire RAM_reg_bram_20_n_30;
  wire RAM_reg_bram_20_n_31;
  wire RAM_reg_bram_20_n_32;
  wire RAM_reg_bram_20_n_33;
  wire RAM_reg_bram_20_n_34;
  wire RAM_reg_bram_20_n_35;
  wire RAM_reg_bram_20_n_60;
  wire RAM_reg_bram_20_n_61;
  wire RAM_reg_bram_20_n_62;
  wire RAM_reg_bram_20_n_63;
  wire RAM_reg_bram_20_n_64;
  wire RAM_reg_bram_20_n_65;
  wire RAM_reg_bram_20_n_66;
  wire RAM_reg_bram_20_n_67;
  wire RAM_reg_bram_21_i_1_n_0;
  wire RAM_reg_bram_21_i_2_n_0;
  wire RAM_reg_bram_21_i_3_n_0;
  wire RAM_reg_bram_21_i_4_n_0;
  wire RAM_reg_bram_21_n_0;
  wire RAM_reg_bram_21_n_1;
  wire RAM_reg_bram_21_n_132;
  wire RAM_reg_bram_21_n_133;
  wire RAM_reg_bram_21_n_134;
  wire RAM_reg_bram_21_n_135;
  wire RAM_reg_bram_21_n_136;
  wire RAM_reg_bram_21_n_137;
  wire RAM_reg_bram_21_n_138;
  wire RAM_reg_bram_21_n_139;
  wire RAM_reg_bram_21_n_28;
  wire RAM_reg_bram_21_n_29;
  wire RAM_reg_bram_21_n_30;
  wire RAM_reg_bram_21_n_31;
  wire RAM_reg_bram_21_n_32;
  wire RAM_reg_bram_21_n_33;
  wire RAM_reg_bram_21_n_34;
  wire RAM_reg_bram_21_n_35;
  wire RAM_reg_bram_21_n_60;
  wire RAM_reg_bram_21_n_61;
  wire RAM_reg_bram_21_n_62;
  wire RAM_reg_bram_21_n_63;
  wire RAM_reg_bram_21_n_64;
  wire RAM_reg_bram_21_n_65;
  wire RAM_reg_bram_21_n_66;
  wire RAM_reg_bram_21_n_67;
  wire [7:0]RAM_reg_bram_22_0;
  wire RAM_reg_bram_22_i_1_n_0;
  wire RAM_reg_bram_22_i_2_n_0;
  wire RAM_reg_bram_22_i_3_n_0;
  wire RAM_reg_bram_22_i_4_n_0;
  wire RAM_reg_bram_22_n_124;
  wire RAM_reg_bram_22_n_125;
  wire RAM_reg_bram_22_n_126;
  wire RAM_reg_bram_22_n_127;
  wire RAM_reg_bram_22_n_128;
  wire RAM_reg_bram_22_n_129;
  wire RAM_reg_bram_22_n_130;
  wire RAM_reg_bram_22_n_131;
  wire RAM_reg_bram_22_n_92;
  wire RAM_reg_bram_22_n_93;
  wire RAM_reg_bram_22_n_94;
  wire RAM_reg_bram_22_n_95;
  wire RAM_reg_bram_22_n_96;
  wire RAM_reg_bram_22_n_97;
  wire RAM_reg_bram_22_n_98;
  wire RAM_reg_bram_22_n_99;
  wire RAM_reg_bram_4_i_1_n_0;
  wire RAM_reg_bram_4_i_2_n_0;
  wire RAM_reg_bram_4_i_3_n_0;
  wire RAM_reg_bram_4_i_4_n_0;
  wire RAM_reg_bram_4_n_0;
  wire RAM_reg_bram_4_n_1;
  wire RAM_reg_bram_4_n_132;
  wire RAM_reg_bram_4_n_133;
  wire RAM_reg_bram_4_n_134;
  wire RAM_reg_bram_4_n_135;
  wire RAM_reg_bram_4_n_136;
  wire RAM_reg_bram_4_n_137;
  wire RAM_reg_bram_4_n_138;
  wire RAM_reg_bram_4_n_139;
  wire RAM_reg_bram_4_n_28;
  wire RAM_reg_bram_4_n_29;
  wire RAM_reg_bram_4_n_30;
  wire RAM_reg_bram_4_n_31;
  wire RAM_reg_bram_4_n_32;
  wire RAM_reg_bram_4_n_33;
  wire RAM_reg_bram_4_n_34;
  wire RAM_reg_bram_4_n_35;
  wire RAM_reg_bram_4_n_60;
  wire RAM_reg_bram_4_n_61;
  wire RAM_reg_bram_4_n_62;
  wire RAM_reg_bram_4_n_63;
  wire RAM_reg_bram_4_n_64;
  wire RAM_reg_bram_4_n_65;
  wire RAM_reg_bram_4_n_66;
  wire RAM_reg_bram_4_n_67;
  wire RAM_reg_bram_5_i_1_n_0;
  wire RAM_reg_bram_5_i_2_n_0;
  wire RAM_reg_bram_5_i_3_n_0;
  wire RAM_reg_bram_5_i_4_n_0;
  wire RAM_reg_bram_5_i_5_n_0;
  wire RAM_reg_bram_5_i_6_n_0;
  wire RAM_reg_bram_5_n_0;
  wire RAM_reg_bram_5_n_1;
  wire RAM_reg_bram_5_n_132;
  wire RAM_reg_bram_5_n_133;
  wire RAM_reg_bram_5_n_134;
  wire RAM_reg_bram_5_n_135;
  wire RAM_reg_bram_5_n_136;
  wire RAM_reg_bram_5_n_137;
  wire RAM_reg_bram_5_n_138;
  wire RAM_reg_bram_5_n_139;
  wire RAM_reg_bram_5_n_28;
  wire RAM_reg_bram_5_n_29;
  wire RAM_reg_bram_5_n_30;
  wire RAM_reg_bram_5_n_31;
  wire RAM_reg_bram_5_n_32;
  wire RAM_reg_bram_5_n_33;
  wire RAM_reg_bram_5_n_34;
  wire RAM_reg_bram_5_n_35;
  wire RAM_reg_bram_5_n_60;
  wire RAM_reg_bram_5_n_61;
  wire RAM_reg_bram_5_n_62;
  wire RAM_reg_bram_5_n_63;
  wire RAM_reg_bram_5_n_64;
  wire RAM_reg_bram_5_n_65;
  wire RAM_reg_bram_5_n_66;
  wire RAM_reg_bram_5_n_67;
  wire RAM_reg_bram_6_i_1_n_0;
  wire RAM_reg_bram_6_i_2_n_0;
  wire RAM_reg_bram_6_i_3_n_0;
  wire RAM_reg_bram_6_i_4_n_0;
  wire RAM_reg_bram_6_i_5_n_0;
  wire RAM_reg_bram_6_i_6_n_0;
  wire RAM_reg_bram_6_n_0;
  wire RAM_reg_bram_6_n_1;
  wire RAM_reg_bram_6_n_132;
  wire RAM_reg_bram_6_n_133;
  wire RAM_reg_bram_6_n_134;
  wire RAM_reg_bram_6_n_135;
  wire RAM_reg_bram_6_n_136;
  wire RAM_reg_bram_6_n_137;
  wire RAM_reg_bram_6_n_138;
  wire RAM_reg_bram_6_n_139;
  wire RAM_reg_bram_6_n_28;
  wire RAM_reg_bram_6_n_29;
  wire RAM_reg_bram_6_n_30;
  wire RAM_reg_bram_6_n_31;
  wire RAM_reg_bram_6_n_32;
  wire RAM_reg_bram_6_n_33;
  wire RAM_reg_bram_6_n_34;
  wire RAM_reg_bram_6_n_35;
  wire RAM_reg_bram_6_n_60;
  wire RAM_reg_bram_6_n_61;
  wire RAM_reg_bram_6_n_62;
  wire RAM_reg_bram_6_n_63;
  wire RAM_reg_bram_6_n_64;
  wire RAM_reg_bram_6_n_65;
  wire RAM_reg_bram_6_n_66;
  wire RAM_reg_bram_6_n_67;
  wire RAM_reg_bram_7_i_1_n_0;
  wire RAM_reg_bram_7_i_2_n_0;
  wire RAM_reg_bram_7_i_3_n_0;
  wire RAM_reg_bram_7_i_4_n_0;
  wire RAM_reg_bram_7_i_5_n_0;
  wire RAM_reg_bram_7_i_6_n_0;
  wire RAM_reg_bram_7_n_0;
  wire RAM_reg_bram_7_n_1;
  wire RAM_reg_bram_7_n_132;
  wire RAM_reg_bram_7_n_133;
  wire RAM_reg_bram_7_n_134;
  wire RAM_reg_bram_7_n_135;
  wire RAM_reg_bram_7_n_136;
  wire RAM_reg_bram_7_n_137;
  wire RAM_reg_bram_7_n_138;
  wire RAM_reg_bram_7_n_139;
  wire RAM_reg_bram_7_n_28;
  wire RAM_reg_bram_7_n_29;
  wire RAM_reg_bram_7_n_30;
  wire RAM_reg_bram_7_n_31;
  wire RAM_reg_bram_7_n_32;
  wire RAM_reg_bram_7_n_33;
  wire RAM_reg_bram_7_n_34;
  wire RAM_reg_bram_7_n_35;
  wire RAM_reg_bram_7_n_60;
  wire RAM_reg_bram_7_n_61;
  wire RAM_reg_bram_7_n_62;
  wire RAM_reg_bram_7_n_63;
  wire RAM_reg_bram_7_n_64;
  wire RAM_reg_bram_7_n_65;
  wire RAM_reg_bram_7_n_66;
  wire RAM_reg_bram_7_n_67;
  wire RAM_reg_bram_8_i_1_n_0;
  wire RAM_reg_bram_8_i_2_n_0;
  wire RAM_reg_bram_8_i_3_n_0;
  wire RAM_reg_bram_8_i_4_n_0;
  wire RAM_reg_bram_8_i_5_n_0;
  wire RAM_reg_bram_8_i_6_n_0;
  wire RAM_reg_bram_8_n_0;
  wire RAM_reg_bram_8_n_1;
  wire RAM_reg_bram_8_n_132;
  wire RAM_reg_bram_8_n_133;
  wire RAM_reg_bram_8_n_134;
  wire RAM_reg_bram_8_n_135;
  wire RAM_reg_bram_8_n_136;
  wire RAM_reg_bram_8_n_137;
  wire RAM_reg_bram_8_n_138;
  wire RAM_reg_bram_8_n_139;
  wire RAM_reg_bram_8_n_28;
  wire RAM_reg_bram_8_n_29;
  wire RAM_reg_bram_8_n_30;
  wire RAM_reg_bram_8_n_31;
  wire RAM_reg_bram_8_n_32;
  wire RAM_reg_bram_8_n_33;
  wire RAM_reg_bram_8_n_34;
  wire RAM_reg_bram_8_n_35;
  wire RAM_reg_bram_8_n_60;
  wire RAM_reg_bram_8_n_61;
  wire RAM_reg_bram_8_n_62;
  wire RAM_reg_bram_8_n_63;
  wire RAM_reg_bram_8_n_64;
  wire RAM_reg_bram_8_n_65;
  wire RAM_reg_bram_8_n_66;
  wire RAM_reg_bram_8_n_67;
  wire RAM_reg_bram_9_i_1_n_0;
  wire RAM_reg_bram_9_i_2_n_0;
  wire RAM_reg_bram_9_i_3_n_0;
  wire RAM_reg_bram_9_i_4_n_0;
  wire RAM_reg_bram_9_i_5_n_0;
  wire RAM_reg_bram_9_i_6_n_0;
  wire RAM_reg_bram_9_n_0;
  wire RAM_reg_bram_9_n_1;
  wire RAM_reg_bram_9_n_132;
  wire RAM_reg_bram_9_n_133;
  wire RAM_reg_bram_9_n_134;
  wire RAM_reg_bram_9_n_135;
  wire RAM_reg_bram_9_n_136;
  wire RAM_reg_bram_9_n_137;
  wire RAM_reg_bram_9_n_138;
  wire RAM_reg_bram_9_n_139;
  wire RAM_reg_bram_9_n_28;
  wire RAM_reg_bram_9_n_29;
  wire RAM_reg_bram_9_n_30;
  wire RAM_reg_bram_9_n_31;
  wire RAM_reg_bram_9_n_32;
  wire RAM_reg_bram_9_n_33;
  wire RAM_reg_bram_9_n_34;
  wire RAM_reg_bram_9_n_35;
  wire RAM_reg_bram_9_n_60;
  wire RAM_reg_bram_9_n_61;
  wire RAM_reg_bram_9_n_62;
  wire RAM_reg_bram_9_n_63;
  wire RAM_reg_bram_9_n_64;
  wire RAM_reg_bram_9_n_65;
  wire RAM_reg_bram_9_n_66;
  wire RAM_reg_bram_9_n_67;
  wire RAM_reg_mux_sel_reg_2_n_0;
  wire [16:0]RAM_reg_mux_sel_reg_3_0;
  wire RAM_reg_mux_sel_reg_3_n_0;
  wire [0:0]data0;
  wire [7:0]dob;
  wire [16:0]i_addr;
  wire i_en;
  wire [7:0]i_px;
  wire i_wen;
  wire o_mf;
  wire s00_axi_aclk;
  wire s_clk;
  wire NLW_RAM_reg_bram_10_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_10_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_10_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_10_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_10_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_10_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_10_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_10_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_10_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_10_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_11_CASOUTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_11_CASOUTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_11_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_11_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_11_CASDOUTA_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_11_CASDOUTB_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_11_CASDOUTPA_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_11_CASDOUTPB_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_11_DOUTADOUT_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_11_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_11_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_11_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_11_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_11_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_12_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_12_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_12_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_12_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_12_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_12_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_12_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_12_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_12_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_12_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_13_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_13_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_13_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_13_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_13_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_13_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_13_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_13_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_13_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_13_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_14_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_14_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_14_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_14_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_14_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_14_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_14_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_14_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_14_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_14_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_15_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_15_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_15_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_15_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_15_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_15_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_15_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_15_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_15_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_15_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_16_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_16_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_16_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_16_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_16_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_16_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_16_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_16_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_16_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_16_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_17_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_17_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_17_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_17_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_17_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_17_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_17_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_17_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_17_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_17_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_18_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_18_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_18_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_18_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_18_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_18_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_18_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_18_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_18_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_18_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_19_CASOUTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_19_CASOUTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_19_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_19_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_19_CASDOUTA_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_19_CASDOUTB_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_19_CASDOUTPA_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_19_CASDOUTPB_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_19_DOUTADOUT_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_19_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_19_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_19_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_19_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_19_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_20_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_20_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_20_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_20_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_20_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_20_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_20_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_20_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_20_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_20_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_21_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_21_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_21_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_21_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_21_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_21_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_21_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_21_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_21_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_21_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_22_CASOUTDBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_22_CASOUTSBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_22_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_22_SBITERR_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_22_CASDOUTA_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_22_CASDOUTB_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_22_CASDOUTPA_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_22_CASDOUTPB_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_22_DOUTADOUT_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_22_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_22_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_22_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_22_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_22_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_4_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_4_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_4_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_4_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_4_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_4_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_4_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_4_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_4_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_4_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_5_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_5_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_5_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_5_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_5_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_5_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_5_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_5_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_5_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_5_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_6_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_6_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_6_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_6_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_6_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_6_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_6_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_6_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_6_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_6_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_7_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_7_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_7_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_7_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_7_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_7_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_7_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_7_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_7_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_7_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_8_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_8_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_8_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_8_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_8_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_8_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_8_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_8_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_8_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_8_RDADDRECC_UNCONNECTED;
  wire NLW_RAM_reg_bram_9_DBITERR_UNCONNECTED;
  wire NLW_RAM_reg_bram_9_SBITERR_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_9_CASDOUTA_UNCONNECTED;
  wire [31:8]NLW_RAM_reg_bram_9_CASDOUTB_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_9_DOUTADOUT_UNCONNECTED;
  wire [31:0]NLW_RAM_reg_bram_9_DOUTBDOUT_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_9_DOUTPADOUTP_UNCONNECTED;
  wire [3:0]NLW_RAM_reg_bram_9_DOUTPBDOUTP_UNCONNECTED;
  wire [7:0]NLW_RAM_reg_bram_9_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_RAM_reg_bram_9_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "24576" *) 
  (* bram_addr_end = "28671" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "24576" *) 
  (* ram_addr_end = "28671" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_10
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_9_n_28,RAM_reg_bram_9_n_29,RAM_reg_bram_9_n_30,RAM_reg_bram_9_n_31,RAM_reg_bram_9_n_32,RAM_reg_bram_9_n_33,RAM_reg_bram_9_n_34,RAM_reg_bram_9_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_9_n_60,RAM_reg_bram_9_n_61,RAM_reg_bram_9_n_62,RAM_reg_bram_9_n_63,RAM_reg_bram_9_n_64,RAM_reg_bram_9_n_65,RAM_reg_bram_9_n_66,RAM_reg_bram_9_n_67}),
        .CASDINPA({RAM_reg_bram_9_n_132,RAM_reg_bram_9_n_133,RAM_reg_bram_9_n_134,RAM_reg_bram_9_n_135}),
        .CASDINPB({RAM_reg_bram_9_n_136,RAM_reg_bram_9_n_137,RAM_reg_bram_9_n_138,RAM_reg_bram_9_n_139}),
        .CASDOMUXA(RAM_reg_bram_10_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_10_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_10_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_10_n_28,RAM_reg_bram_10_n_29,RAM_reg_bram_10_n_30,RAM_reg_bram_10_n_31,RAM_reg_bram_10_n_32,RAM_reg_bram_10_n_33,RAM_reg_bram_10_n_34,RAM_reg_bram_10_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_10_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_10_n_60,RAM_reg_bram_10_n_61,RAM_reg_bram_10_n_62,RAM_reg_bram_10_n_63,RAM_reg_bram_10_n_64,RAM_reg_bram_10_n_65,RAM_reg_bram_10_n_66,RAM_reg_bram_10_n_67}),
        .CASDOUTPA({RAM_reg_bram_10_n_132,RAM_reg_bram_10_n_133,RAM_reg_bram_10_n_134,RAM_reg_bram_10_n_135}),
        .CASDOUTPB({RAM_reg_bram_10_n_136,RAM_reg_bram_10_n_137,RAM_reg_bram_10_n_138,RAM_reg_bram_10_n_139}),
        .CASINDBITERR(RAM_reg_bram_9_n_0),
        .CASINSBITERR(RAM_reg_bram_9_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_10_n_0),
        .CASOUTSBITERR(RAM_reg_bram_10_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_10_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_10_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_10_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_10_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_10_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_10_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_10_i_3_n_0),
        .ENBWREN(RAM_reg_bram_10_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_10_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_10_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_10_i_5_n_0,RAM_reg_bram_10_i_5_n_0,RAM_reg_bram_10_i_5_n_0,RAM_reg_bram_10_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_10_i_6_n_0,RAM_reg_bram_10_i_6_n_0,RAM_reg_bram_10_i_6_n_0,RAM_reg_bram_10_i_6_n_0}));
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_10_i_1
       (.I0(i_addr[12]),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .O(RAM_reg_bram_10_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_10_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[12]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_10_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_10_i_3
       (.I0(i_en),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_10_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_10_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_10_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_10_i_5
       (.I0(i_wen),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_10_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_10_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_10_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "28672" *) 
  (* bram_addr_end = "32767" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "28672" *) 
  (* ram_addr_end = "32767" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_11
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_10_n_28,RAM_reg_bram_10_n_29,RAM_reg_bram_10_n_30,RAM_reg_bram_10_n_31,RAM_reg_bram_10_n_32,RAM_reg_bram_10_n_33,RAM_reg_bram_10_n_34,RAM_reg_bram_10_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_10_n_60,RAM_reg_bram_10_n_61,RAM_reg_bram_10_n_62,RAM_reg_bram_10_n_63,RAM_reg_bram_10_n_64,RAM_reg_bram_10_n_65,RAM_reg_bram_10_n_66,RAM_reg_bram_10_n_67}),
        .CASDINPA({RAM_reg_bram_10_n_132,RAM_reg_bram_10_n_133,RAM_reg_bram_10_n_134,RAM_reg_bram_10_n_135}),
        .CASDINPB({RAM_reg_bram_10_n_136,RAM_reg_bram_10_n_137,RAM_reg_bram_10_n_138,RAM_reg_bram_10_n_139}),
        .CASDOMUXA(RAM_reg_bram_11_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_11_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA(NLW_RAM_reg_bram_11_CASDOUTA_UNCONNECTED[31:0]),
        .CASDOUTB(NLW_RAM_reg_bram_11_CASDOUTB_UNCONNECTED[31:0]),
        .CASDOUTPA(NLW_RAM_reg_bram_11_CASDOUTPA_UNCONNECTED[3:0]),
        .CASDOUTPB(NLW_RAM_reg_bram_11_CASDOUTPB_UNCONNECTED[3:0]),
        .CASINDBITERR(RAM_reg_bram_10_n_0),
        .CASINSBITERR(RAM_reg_bram_10_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(NLW_RAM_reg_bram_11_CASOUTDBITERR_UNCONNECTED),
        .CASOUTSBITERR(NLW_RAM_reg_bram_11_CASOUTSBITERR_UNCONNECTED),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_11_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({NLW_RAM_reg_bram_11_DOUTADOUT_UNCONNECTED[31:8],RAM_reg_bram_11_n_92,RAM_reg_bram_11_n_93,RAM_reg_bram_11_n_94,RAM_reg_bram_11_n_95,RAM_reg_bram_11_n_96,RAM_reg_bram_11_n_97,RAM_reg_bram_11_n_98,RAM_reg_bram_11_n_99}),
        .DOUTBDOUT({NLW_RAM_reg_bram_11_DOUTBDOUT_UNCONNECTED[31:8],RAM_reg_bram_11_n_124,RAM_reg_bram_11_n_125,RAM_reg_bram_11_n_126,RAM_reg_bram_11_n_127,RAM_reg_bram_11_n_128,RAM_reg_bram_11_n_129,RAM_reg_bram_11_n_130,RAM_reg_bram_11_n_131}),
        .DOUTPADOUTP(NLW_RAM_reg_bram_11_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_11_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_11_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_11_i_3_n_0),
        .ENBWREN(RAM_reg_bram_11_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_11_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_11_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_11_i_5_n_0,RAM_reg_bram_11_i_5_n_0,RAM_reg_bram_11_i_5_n_0,RAM_reg_bram_11_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_11_i_6_n_0,RAM_reg_bram_11_i_6_n_0,RAM_reg_bram_11_i_6_n_0,RAM_reg_bram_11_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    RAM_reg_bram_11_i_1
       (.I0(i_addr[12]),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .O(RAM_reg_bram_11_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    RAM_reg_bram_11_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[12]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_11_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_11_i_3
       (.I0(i_en),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[14]),
        .I4(i_addr[13]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_11_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_11_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[14]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_11_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_11_i_5
       (.I0(i_wen),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[14]),
        .I4(i_addr[13]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_11_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_11_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[14]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_11_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "32768" *) 
  (* bram_addr_end = "36863" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "32768" *) 
  (* ram_addr_end = "36863" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_12
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({NLW_RAM_reg_bram_12_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_12_n_28,RAM_reg_bram_12_n_29,RAM_reg_bram_12_n_30,RAM_reg_bram_12_n_31,RAM_reg_bram_12_n_32,RAM_reg_bram_12_n_33,RAM_reg_bram_12_n_34,RAM_reg_bram_12_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_12_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_12_n_60,RAM_reg_bram_12_n_61,RAM_reg_bram_12_n_62,RAM_reg_bram_12_n_63,RAM_reg_bram_12_n_64,RAM_reg_bram_12_n_65,RAM_reg_bram_12_n_66,RAM_reg_bram_12_n_67}),
        .CASDOUTPA({RAM_reg_bram_12_n_132,RAM_reg_bram_12_n_133,RAM_reg_bram_12_n_134,RAM_reg_bram_12_n_135}),
        .CASDOUTPB({RAM_reg_bram_12_n_136,RAM_reg_bram_12_n_137,RAM_reg_bram_12_n_138,RAM_reg_bram_12_n_139}),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_12_n_0),
        .CASOUTSBITERR(RAM_reg_bram_12_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_12_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_12_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_12_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_12_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_12_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_12_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_12_i_1_n_0),
        .ENBWREN(RAM_reg_bram_12_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_12_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_12_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_12_i_3_n_0,RAM_reg_bram_12_i_3_n_0,RAM_reg_bram_12_i_3_n_0,RAM_reg_bram_12_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_12_i_4_n_0,RAM_reg_bram_12_i_4_n_0,RAM_reg_bram_12_i_4_n_0,RAM_reg_bram_12_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_12_i_1
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_12_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_12_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_12_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_12_i_3
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_12_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_12_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_12_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "36864" *) 
  (* bram_addr_end = "40959" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "36864" *) 
  (* ram_addr_end = "40959" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_13
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_12_n_28,RAM_reg_bram_12_n_29,RAM_reg_bram_12_n_30,RAM_reg_bram_12_n_31,RAM_reg_bram_12_n_32,RAM_reg_bram_12_n_33,RAM_reg_bram_12_n_34,RAM_reg_bram_12_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_12_n_60,RAM_reg_bram_12_n_61,RAM_reg_bram_12_n_62,RAM_reg_bram_12_n_63,RAM_reg_bram_12_n_64,RAM_reg_bram_12_n_65,RAM_reg_bram_12_n_66,RAM_reg_bram_12_n_67}),
        .CASDINPA({RAM_reg_bram_12_n_132,RAM_reg_bram_12_n_133,RAM_reg_bram_12_n_134,RAM_reg_bram_12_n_135}),
        .CASDINPB({RAM_reg_bram_12_n_136,RAM_reg_bram_12_n_137,RAM_reg_bram_12_n_138,RAM_reg_bram_12_n_139}),
        .CASDOMUXA(RAM_reg_bram_5_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_5_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_13_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_13_n_28,RAM_reg_bram_13_n_29,RAM_reg_bram_13_n_30,RAM_reg_bram_13_n_31,RAM_reg_bram_13_n_32,RAM_reg_bram_13_n_33,RAM_reg_bram_13_n_34,RAM_reg_bram_13_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_13_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_13_n_60,RAM_reg_bram_13_n_61,RAM_reg_bram_13_n_62,RAM_reg_bram_13_n_63,RAM_reg_bram_13_n_64,RAM_reg_bram_13_n_65,RAM_reg_bram_13_n_66,RAM_reg_bram_13_n_67}),
        .CASDOUTPA({RAM_reg_bram_13_n_132,RAM_reg_bram_13_n_133,RAM_reg_bram_13_n_134,RAM_reg_bram_13_n_135}),
        .CASDOUTPB({RAM_reg_bram_13_n_136,RAM_reg_bram_13_n_137,RAM_reg_bram_13_n_138,RAM_reg_bram_13_n_139}),
        .CASINDBITERR(RAM_reg_bram_12_n_0),
        .CASINSBITERR(RAM_reg_bram_12_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_13_n_0),
        .CASOUTSBITERR(RAM_reg_bram_13_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_13_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_13_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_13_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_13_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_13_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_13_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_13_i_1_n_0),
        .ENBWREN(RAM_reg_bram_13_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_13_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_13_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_13_i_3_n_0,RAM_reg_bram_13_i_3_n_0,RAM_reg_bram_13_i_3_n_0,RAM_reg_bram_13_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_13_i_4_n_0,RAM_reg_bram_13_i_4_n_0,RAM_reg_bram_13_i_4_n_0,RAM_reg_bram_13_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_13_i_1
       (.I0(i_en),
        .I1(i_addr[15]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_13_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_13_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_13_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_13_i_3
       (.I0(i_wen),
        .I1(i_addr[15]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_13_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_13_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_13_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "40960" *) 
  (* bram_addr_end = "45055" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "40960" *) 
  (* ram_addr_end = "45055" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_14
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_13_n_28,RAM_reg_bram_13_n_29,RAM_reg_bram_13_n_30,RAM_reg_bram_13_n_31,RAM_reg_bram_13_n_32,RAM_reg_bram_13_n_33,RAM_reg_bram_13_n_34,RAM_reg_bram_13_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_13_n_60,RAM_reg_bram_13_n_61,RAM_reg_bram_13_n_62,RAM_reg_bram_13_n_63,RAM_reg_bram_13_n_64,RAM_reg_bram_13_n_65,RAM_reg_bram_13_n_66,RAM_reg_bram_13_n_67}),
        .CASDINPA({RAM_reg_bram_13_n_132,RAM_reg_bram_13_n_133,RAM_reg_bram_13_n_134,RAM_reg_bram_13_n_135}),
        .CASDINPB({RAM_reg_bram_13_n_136,RAM_reg_bram_13_n_137,RAM_reg_bram_13_n_138,RAM_reg_bram_13_n_139}),
        .CASDOMUXA(RAM_reg_bram_6_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_6_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_14_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_14_n_28,RAM_reg_bram_14_n_29,RAM_reg_bram_14_n_30,RAM_reg_bram_14_n_31,RAM_reg_bram_14_n_32,RAM_reg_bram_14_n_33,RAM_reg_bram_14_n_34,RAM_reg_bram_14_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_14_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_14_n_60,RAM_reg_bram_14_n_61,RAM_reg_bram_14_n_62,RAM_reg_bram_14_n_63,RAM_reg_bram_14_n_64,RAM_reg_bram_14_n_65,RAM_reg_bram_14_n_66,RAM_reg_bram_14_n_67}),
        .CASDOUTPA({RAM_reg_bram_14_n_132,RAM_reg_bram_14_n_133,RAM_reg_bram_14_n_134,RAM_reg_bram_14_n_135}),
        .CASDOUTPB({RAM_reg_bram_14_n_136,RAM_reg_bram_14_n_137,RAM_reg_bram_14_n_138,RAM_reg_bram_14_n_139}),
        .CASINDBITERR(RAM_reg_bram_13_n_0),
        .CASINSBITERR(RAM_reg_bram_13_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_14_n_0),
        .CASOUTSBITERR(RAM_reg_bram_14_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_14_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_14_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_14_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_14_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_14_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_14_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_14_i_1_n_0),
        .ENBWREN(RAM_reg_bram_14_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_14_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_14_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_14_i_3_n_0,RAM_reg_bram_14_i_3_n_0,RAM_reg_bram_14_i_3_n_0,RAM_reg_bram_14_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_14_i_4_n_0,RAM_reg_bram_14_i_4_n_0,RAM_reg_bram_14_i_4_n_0,RAM_reg_bram_14_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_14_i_1
       (.I0(i_en),
        .I1(i_addr[15]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_14_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_14_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_14_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_14_i_3
       (.I0(i_wen),
        .I1(i_addr[15]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_14_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_14_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_14_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "45056" *) 
  (* bram_addr_end = "49151" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "45056" *) 
  (* ram_addr_end = "49151" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_15
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_14_n_28,RAM_reg_bram_14_n_29,RAM_reg_bram_14_n_30,RAM_reg_bram_14_n_31,RAM_reg_bram_14_n_32,RAM_reg_bram_14_n_33,RAM_reg_bram_14_n_34,RAM_reg_bram_14_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_14_n_60,RAM_reg_bram_14_n_61,RAM_reg_bram_14_n_62,RAM_reg_bram_14_n_63,RAM_reg_bram_14_n_64,RAM_reg_bram_14_n_65,RAM_reg_bram_14_n_66,RAM_reg_bram_14_n_67}),
        .CASDINPA({RAM_reg_bram_14_n_132,RAM_reg_bram_14_n_133,RAM_reg_bram_14_n_134,RAM_reg_bram_14_n_135}),
        .CASDINPB({RAM_reg_bram_14_n_136,RAM_reg_bram_14_n_137,RAM_reg_bram_14_n_138,RAM_reg_bram_14_n_139}),
        .CASDOMUXA(RAM_reg_bram_7_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_7_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_15_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_15_n_28,RAM_reg_bram_15_n_29,RAM_reg_bram_15_n_30,RAM_reg_bram_15_n_31,RAM_reg_bram_15_n_32,RAM_reg_bram_15_n_33,RAM_reg_bram_15_n_34,RAM_reg_bram_15_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_15_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_15_n_60,RAM_reg_bram_15_n_61,RAM_reg_bram_15_n_62,RAM_reg_bram_15_n_63,RAM_reg_bram_15_n_64,RAM_reg_bram_15_n_65,RAM_reg_bram_15_n_66,RAM_reg_bram_15_n_67}),
        .CASDOUTPA({RAM_reg_bram_15_n_132,RAM_reg_bram_15_n_133,RAM_reg_bram_15_n_134,RAM_reg_bram_15_n_135}),
        .CASDOUTPB({RAM_reg_bram_15_n_136,RAM_reg_bram_15_n_137,RAM_reg_bram_15_n_138,RAM_reg_bram_15_n_139}),
        .CASINDBITERR(RAM_reg_bram_14_n_0),
        .CASINSBITERR(RAM_reg_bram_14_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_15_n_0),
        .CASOUTSBITERR(RAM_reg_bram_15_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_15_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_15_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_15_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_15_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_15_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_15_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_15_i_1_n_0),
        .ENBWREN(RAM_reg_bram_15_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_15_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_15_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_15_i_3_n_0,RAM_reg_bram_15_i_3_n_0,RAM_reg_bram_15_i_3_n_0,RAM_reg_bram_15_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_15_i_4_n_0,RAM_reg_bram_15_i_4_n_0,RAM_reg_bram_15_i_4_n_0,RAM_reg_bram_15_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_15_i_1
       (.I0(i_en),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[13]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_15_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_15_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_15_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_15_i_3
       (.I0(i_wen),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[13]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_15_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_15_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_15_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "49152" *) 
  (* bram_addr_end = "53247" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "49152" *) 
  (* ram_addr_end = "53247" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_16
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_15_n_28,RAM_reg_bram_15_n_29,RAM_reg_bram_15_n_30,RAM_reg_bram_15_n_31,RAM_reg_bram_15_n_32,RAM_reg_bram_15_n_33,RAM_reg_bram_15_n_34,RAM_reg_bram_15_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_15_n_60,RAM_reg_bram_15_n_61,RAM_reg_bram_15_n_62,RAM_reg_bram_15_n_63,RAM_reg_bram_15_n_64,RAM_reg_bram_15_n_65,RAM_reg_bram_15_n_66,RAM_reg_bram_15_n_67}),
        .CASDINPA({RAM_reg_bram_15_n_132,RAM_reg_bram_15_n_133,RAM_reg_bram_15_n_134,RAM_reg_bram_15_n_135}),
        .CASDINPB({RAM_reg_bram_15_n_136,RAM_reg_bram_15_n_137,RAM_reg_bram_15_n_138,RAM_reg_bram_15_n_139}),
        .CASDOMUXA(RAM_reg_bram_8_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_8_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_16_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_16_n_28,RAM_reg_bram_16_n_29,RAM_reg_bram_16_n_30,RAM_reg_bram_16_n_31,RAM_reg_bram_16_n_32,RAM_reg_bram_16_n_33,RAM_reg_bram_16_n_34,RAM_reg_bram_16_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_16_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_16_n_60,RAM_reg_bram_16_n_61,RAM_reg_bram_16_n_62,RAM_reg_bram_16_n_63,RAM_reg_bram_16_n_64,RAM_reg_bram_16_n_65,RAM_reg_bram_16_n_66,RAM_reg_bram_16_n_67}),
        .CASDOUTPA({RAM_reg_bram_16_n_132,RAM_reg_bram_16_n_133,RAM_reg_bram_16_n_134,RAM_reg_bram_16_n_135}),
        .CASDOUTPB({RAM_reg_bram_16_n_136,RAM_reg_bram_16_n_137,RAM_reg_bram_16_n_138,RAM_reg_bram_16_n_139}),
        .CASINDBITERR(RAM_reg_bram_15_n_0),
        .CASINSBITERR(RAM_reg_bram_15_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_16_n_0),
        .CASOUTSBITERR(RAM_reg_bram_16_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_16_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_16_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_16_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_16_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_16_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_16_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_16_i_1_n_0),
        .ENBWREN(RAM_reg_bram_16_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_16_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_16_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_16_i_3_n_0,RAM_reg_bram_16_i_3_n_0,RAM_reg_bram_16_i_3_n_0,RAM_reg_bram_16_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_16_i_4_n_0,RAM_reg_bram_16_i_4_n_0,RAM_reg_bram_16_i_4_n_0,RAM_reg_bram_16_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_16_i_1
       (.I0(i_en),
        .I1(i_addr[15]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[13]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_16_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_16_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_16_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_16_i_3
       (.I0(i_wen),
        .I1(i_addr[15]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[13]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_16_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_16_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[15]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[13]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_16_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "53248" *) 
  (* bram_addr_end = "57343" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "53248" *) 
  (* ram_addr_end = "57343" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_17
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_16_n_28,RAM_reg_bram_16_n_29,RAM_reg_bram_16_n_30,RAM_reg_bram_16_n_31,RAM_reg_bram_16_n_32,RAM_reg_bram_16_n_33,RAM_reg_bram_16_n_34,RAM_reg_bram_16_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_16_n_60,RAM_reg_bram_16_n_61,RAM_reg_bram_16_n_62,RAM_reg_bram_16_n_63,RAM_reg_bram_16_n_64,RAM_reg_bram_16_n_65,RAM_reg_bram_16_n_66,RAM_reg_bram_16_n_67}),
        .CASDINPA({RAM_reg_bram_16_n_132,RAM_reg_bram_16_n_133,RAM_reg_bram_16_n_134,RAM_reg_bram_16_n_135}),
        .CASDINPB({RAM_reg_bram_16_n_136,RAM_reg_bram_16_n_137,RAM_reg_bram_16_n_138,RAM_reg_bram_16_n_139}),
        .CASDOMUXA(RAM_reg_bram_9_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_9_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_17_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_17_n_28,RAM_reg_bram_17_n_29,RAM_reg_bram_17_n_30,RAM_reg_bram_17_n_31,RAM_reg_bram_17_n_32,RAM_reg_bram_17_n_33,RAM_reg_bram_17_n_34,RAM_reg_bram_17_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_17_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_17_n_60,RAM_reg_bram_17_n_61,RAM_reg_bram_17_n_62,RAM_reg_bram_17_n_63,RAM_reg_bram_17_n_64,RAM_reg_bram_17_n_65,RAM_reg_bram_17_n_66,RAM_reg_bram_17_n_67}),
        .CASDOUTPA({RAM_reg_bram_17_n_132,RAM_reg_bram_17_n_133,RAM_reg_bram_17_n_134,RAM_reg_bram_17_n_135}),
        .CASDOUTPB({RAM_reg_bram_17_n_136,RAM_reg_bram_17_n_137,RAM_reg_bram_17_n_138,RAM_reg_bram_17_n_139}),
        .CASINDBITERR(RAM_reg_bram_16_n_0),
        .CASINSBITERR(RAM_reg_bram_16_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_17_n_0),
        .CASOUTSBITERR(RAM_reg_bram_17_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_17_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_17_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_17_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_17_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_17_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_17_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_17_i_1_n_0),
        .ENBWREN(RAM_reg_bram_17_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_17_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_17_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_17_i_3_n_0,RAM_reg_bram_17_i_3_n_0,RAM_reg_bram_17_i_3_n_0,RAM_reg_bram_17_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_17_i_4_n_0,RAM_reg_bram_17_i_4_n_0,RAM_reg_bram_17_i_4_n_0,RAM_reg_bram_17_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_17_i_1
       (.I0(i_en),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_17_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_17_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_17_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_17_i_3
       (.I0(i_wen),
        .I1(i_addr[12]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_17_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_17_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_17_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "57344" *) 
  (* bram_addr_end = "61439" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "57344" *) 
  (* ram_addr_end = "61439" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_18
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_17_n_28,RAM_reg_bram_17_n_29,RAM_reg_bram_17_n_30,RAM_reg_bram_17_n_31,RAM_reg_bram_17_n_32,RAM_reg_bram_17_n_33,RAM_reg_bram_17_n_34,RAM_reg_bram_17_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_17_n_60,RAM_reg_bram_17_n_61,RAM_reg_bram_17_n_62,RAM_reg_bram_17_n_63,RAM_reg_bram_17_n_64,RAM_reg_bram_17_n_65,RAM_reg_bram_17_n_66,RAM_reg_bram_17_n_67}),
        .CASDINPA({RAM_reg_bram_17_n_132,RAM_reg_bram_17_n_133,RAM_reg_bram_17_n_134,RAM_reg_bram_17_n_135}),
        .CASDINPB({RAM_reg_bram_17_n_136,RAM_reg_bram_17_n_137,RAM_reg_bram_17_n_138,RAM_reg_bram_17_n_139}),
        .CASDOMUXA(RAM_reg_bram_10_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_10_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_18_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_18_n_28,RAM_reg_bram_18_n_29,RAM_reg_bram_18_n_30,RAM_reg_bram_18_n_31,RAM_reg_bram_18_n_32,RAM_reg_bram_18_n_33,RAM_reg_bram_18_n_34,RAM_reg_bram_18_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_18_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_18_n_60,RAM_reg_bram_18_n_61,RAM_reg_bram_18_n_62,RAM_reg_bram_18_n_63,RAM_reg_bram_18_n_64,RAM_reg_bram_18_n_65,RAM_reg_bram_18_n_66,RAM_reg_bram_18_n_67}),
        .CASDOUTPA({RAM_reg_bram_18_n_132,RAM_reg_bram_18_n_133,RAM_reg_bram_18_n_134,RAM_reg_bram_18_n_135}),
        .CASDOUTPB({RAM_reg_bram_18_n_136,RAM_reg_bram_18_n_137,RAM_reg_bram_18_n_138,RAM_reg_bram_18_n_139}),
        .CASINDBITERR(RAM_reg_bram_17_n_0),
        .CASINSBITERR(RAM_reg_bram_17_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_18_n_0),
        .CASOUTSBITERR(RAM_reg_bram_18_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_18_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_18_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_18_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_18_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_18_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_18_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_18_i_1_n_0),
        .ENBWREN(RAM_reg_bram_18_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_18_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_18_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_18_i_3_n_0,RAM_reg_bram_18_i_3_n_0,RAM_reg_bram_18_i_3_n_0,RAM_reg_bram_18_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_18_i_4_n_0,RAM_reg_bram_18_i_4_n_0,RAM_reg_bram_18_i_4_n_0,RAM_reg_bram_18_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_18_i_1
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_18_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_18_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_18_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_18_i_3
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[16]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_18_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000008000000)) 
    RAM_reg_bram_18_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[16]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_18_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "61440" *) 
  (* bram_addr_end = "65535" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "61440" *) 
  (* ram_addr_end = "65535" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_19
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_18_n_28,RAM_reg_bram_18_n_29,RAM_reg_bram_18_n_30,RAM_reg_bram_18_n_31,RAM_reg_bram_18_n_32,RAM_reg_bram_18_n_33,RAM_reg_bram_18_n_34,RAM_reg_bram_18_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_18_n_60,RAM_reg_bram_18_n_61,RAM_reg_bram_18_n_62,RAM_reg_bram_18_n_63,RAM_reg_bram_18_n_64,RAM_reg_bram_18_n_65,RAM_reg_bram_18_n_66,RAM_reg_bram_18_n_67}),
        .CASDINPA({RAM_reg_bram_18_n_132,RAM_reg_bram_18_n_133,RAM_reg_bram_18_n_134,RAM_reg_bram_18_n_135}),
        .CASDINPB({RAM_reg_bram_18_n_136,RAM_reg_bram_18_n_137,RAM_reg_bram_18_n_138,RAM_reg_bram_18_n_139}),
        .CASDOMUXA(RAM_reg_bram_11_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_11_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA(NLW_RAM_reg_bram_19_CASDOUTA_UNCONNECTED[31:0]),
        .CASDOUTB(NLW_RAM_reg_bram_19_CASDOUTB_UNCONNECTED[31:0]),
        .CASDOUTPA(NLW_RAM_reg_bram_19_CASDOUTPA_UNCONNECTED[3:0]),
        .CASDOUTPB(NLW_RAM_reg_bram_19_CASDOUTPB_UNCONNECTED[3:0]),
        .CASINDBITERR(RAM_reg_bram_18_n_0),
        .CASINSBITERR(RAM_reg_bram_18_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(NLW_RAM_reg_bram_19_CASOUTDBITERR_UNCONNECTED),
        .CASOUTSBITERR(NLW_RAM_reg_bram_19_CASOUTSBITERR_UNCONNECTED),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_19_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({NLW_RAM_reg_bram_19_DOUTADOUT_UNCONNECTED[31:8],RAM_reg_bram_19_n_92,RAM_reg_bram_19_n_93,RAM_reg_bram_19_n_94,RAM_reg_bram_19_n_95,RAM_reg_bram_19_n_96,RAM_reg_bram_19_n_97,RAM_reg_bram_19_n_98,RAM_reg_bram_19_n_99}),
        .DOUTBDOUT({NLW_RAM_reg_bram_19_DOUTBDOUT_UNCONNECTED[31:8],RAM_reg_bram_19_n_124,RAM_reg_bram_19_n_125,RAM_reg_bram_19_n_126,RAM_reg_bram_19_n_127,RAM_reg_bram_19_n_128,RAM_reg_bram_19_n_129,RAM_reg_bram_19_n_130,RAM_reg_bram_19_n_131}),
        .DOUTPADOUTP(NLW_RAM_reg_bram_19_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_19_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_19_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_19_i_1_n_0),
        .ENBWREN(RAM_reg_bram_19_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_19_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_19_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_19_i_3_n_0,RAM_reg_bram_19_i_3_n_0,RAM_reg_bram_19_i_3_n_0,RAM_reg_bram_19_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_19_i_4_n_0,RAM_reg_bram_19_i_4_n_0,RAM_reg_bram_19_i_4_n_0,RAM_reg_bram_19_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    RAM_reg_bram_19_i_1
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[16]),
        .O(RAM_reg_bram_19_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    RAM_reg_bram_19_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[16]),
        .O(RAM_reg_bram_19_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    RAM_reg_bram_19_i_3
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[16]),
        .O(RAM_reg_bram_19_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000080000000)) 
    RAM_reg_bram_19_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[16]),
        .O(RAM_reg_bram_19_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "65536" *) 
  (* bram_addr_end = "69631" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "65536" *) 
  (* ram_addr_end = "69631" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_20
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({NLW_RAM_reg_bram_20_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_20_n_28,RAM_reg_bram_20_n_29,RAM_reg_bram_20_n_30,RAM_reg_bram_20_n_31,RAM_reg_bram_20_n_32,RAM_reg_bram_20_n_33,RAM_reg_bram_20_n_34,RAM_reg_bram_20_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_20_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_20_n_60,RAM_reg_bram_20_n_61,RAM_reg_bram_20_n_62,RAM_reg_bram_20_n_63,RAM_reg_bram_20_n_64,RAM_reg_bram_20_n_65,RAM_reg_bram_20_n_66,RAM_reg_bram_20_n_67}),
        .CASDOUTPA({RAM_reg_bram_20_n_132,RAM_reg_bram_20_n_133,RAM_reg_bram_20_n_134,RAM_reg_bram_20_n_135}),
        .CASDOUTPB({RAM_reg_bram_20_n_136,RAM_reg_bram_20_n_137,RAM_reg_bram_20_n_138,RAM_reg_bram_20_n_139}),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_20_n_0),
        .CASOUTSBITERR(RAM_reg_bram_20_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_20_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_20_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_20_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_20_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_20_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_20_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_20_i_1_n_0),
        .ENBWREN(RAM_reg_bram_20_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_20_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_20_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_20_i_3_n_0,RAM_reg_bram_20_i_3_n_0,RAM_reg_bram_20_i_3_n_0,RAM_reg_bram_20_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_20_i_4_n_0,RAM_reg_bram_20_i_4_n_0,RAM_reg_bram_20_i_4_n_0,RAM_reg_bram_20_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_20_i_1
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[16]),
        .O(RAM_reg_bram_20_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_20_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[16]),
        .O(RAM_reg_bram_20_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_20_i_3
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[16]),
        .O(RAM_reg_bram_20_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_20_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[16]),
        .O(RAM_reg_bram_20_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "69632" *) 
  (* bram_addr_end = "73727" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "69632" *) 
  (* ram_addr_end = "73727" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_21
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_20_n_28,RAM_reg_bram_20_n_29,RAM_reg_bram_20_n_30,RAM_reg_bram_20_n_31,RAM_reg_bram_20_n_32,RAM_reg_bram_20_n_33,RAM_reg_bram_20_n_34,RAM_reg_bram_20_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_20_n_60,RAM_reg_bram_20_n_61,RAM_reg_bram_20_n_62,RAM_reg_bram_20_n_63,RAM_reg_bram_20_n_64,RAM_reg_bram_20_n_65,RAM_reg_bram_20_n_66,RAM_reg_bram_20_n_67}),
        .CASDINPA({RAM_reg_bram_20_n_132,RAM_reg_bram_20_n_133,RAM_reg_bram_20_n_134,RAM_reg_bram_20_n_135}),
        .CASDINPB({RAM_reg_bram_20_n_136,RAM_reg_bram_20_n_137,RAM_reg_bram_20_n_138,RAM_reg_bram_20_n_139}),
        .CASDOMUXA(RAM_reg_bram_5_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_5_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_21_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_21_n_28,RAM_reg_bram_21_n_29,RAM_reg_bram_21_n_30,RAM_reg_bram_21_n_31,RAM_reg_bram_21_n_32,RAM_reg_bram_21_n_33,RAM_reg_bram_21_n_34,RAM_reg_bram_21_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_21_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_21_n_60,RAM_reg_bram_21_n_61,RAM_reg_bram_21_n_62,RAM_reg_bram_21_n_63,RAM_reg_bram_21_n_64,RAM_reg_bram_21_n_65,RAM_reg_bram_21_n_66,RAM_reg_bram_21_n_67}),
        .CASDOUTPA({RAM_reg_bram_21_n_132,RAM_reg_bram_21_n_133,RAM_reg_bram_21_n_134,RAM_reg_bram_21_n_135}),
        .CASDOUTPB({RAM_reg_bram_21_n_136,RAM_reg_bram_21_n_137,RAM_reg_bram_21_n_138,RAM_reg_bram_21_n_139}),
        .CASINDBITERR(RAM_reg_bram_20_n_0),
        .CASINSBITERR(RAM_reg_bram_20_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_21_n_0),
        .CASOUTSBITERR(RAM_reg_bram_21_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_21_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_21_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_21_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_21_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_21_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_21_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_21_i_1_n_0),
        .ENBWREN(RAM_reg_bram_21_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_21_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_21_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_21_i_3_n_0,RAM_reg_bram_21_i_3_n_0,RAM_reg_bram_21_i_3_n_0,RAM_reg_bram_21_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_21_i_4_n_0,RAM_reg_bram_21_i_4_n_0,RAM_reg_bram_21_i_4_n_0,RAM_reg_bram_21_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_21_i_1
       (.I0(i_en),
        .I1(i_addr[16]),
        .I2(i_addr[13]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_21_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_21_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[16]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_21_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_21_i_3
       (.I0(i_wen),
        .I1(i_addr[16]),
        .I2(i_addr[13]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_21_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_21_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[16]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_21_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}} {SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "73728" *) 
  (* bram_addr_end = "77823" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "73728" *) 
  (* ram_addr_end = "77823" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("LAST"),
    .CASCADE_ORDER_B("LAST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_22
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_21_n_28,RAM_reg_bram_21_n_29,RAM_reg_bram_21_n_30,RAM_reg_bram_21_n_31,RAM_reg_bram_21_n_32,RAM_reg_bram_21_n_33,RAM_reg_bram_21_n_34,RAM_reg_bram_21_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_21_n_60,RAM_reg_bram_21_n_61,RAM_reg_bram_21_n_62,RAM_reg_bram_21_n_63,RAM_reg_bram_21_n_64,RAM_reg_bram_21_n_65,RAM_reg_bram_21_n_66,RAM_reg_bram_21_n_67}),
        .CASDINPA({RAM_reg_bram_21_n_132,RAM_reg_bram_21_n_133,RAM_reg_bram_21_n_134,RAM_reg_bram_21_n_135}),
        .CASDINPB({RAM_reg_bram_21_n_136,RAM_reg_bram_21_n_137,RAM_reg_bram_21_n_138,RAM_reg_bram_21_n_139}),
        .CASDOMUXA(RAM_reg_bram_6_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_6_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA(NLW_RAM_reg_bram_22_CASDOUTA_UNCONNECTED[31:0]),
        .CASDOUTB(NLW_RAM_reg_bram_22_CASDOUTB_UNCONNECTED[31:0]),
        .CASDOUTPA(NLW_RAM_reg_bram_22_CASDOUTPA_UNCONNECTED[3:0]),
        .CASDOUTPB(NLW_RAM_reg_bram_22_CASDOUTPB_UNCONNECTED[3:0]),
        .CASINDBITERR(RAM_reg_bram_21_n_0),
        .CASINSBITERR(RAM_reg_bram_21_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(NLW_RAM_reg_bram_22_CASOUTDBITERR_UNCONNECTED),
        .CASOUTSBITERR(NLW_RAM_reg_bram_22_CASOUTSBITERR_UNCONNECTED),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_22_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT({NLW_RAM_reg_bram_22_DOUTADOUT_UNCONNECTED[31:8],RAM_reg_bram_22_n_92,RAM_reg_bram_22_n_93,RAM_reg_bram_22_n_94,RAM_reg_bram_22_n_95,RAM_reg_bram_22_n_96,RAM_reg_bram_22_n_97,RAM_reg_bram_22_n_98,RAM_reg_bram_22_n_99}),
        .DOUTBDOUT({NLW_RAM_reg_bram_22_DOUTBDOUT_UNCONNECTED[31:8],RAM_reg_bram_22_n_124,RAM_reg_bram_22_n_125,RAM_reg_bram_22_n_126,RAM_reg_bram_22_n_127,RAM_reg_bram_22_n_128,RAM_reg_bram_22_n_129,RAM_reg_bram_22_n_130,RAM_reg_bram_22_n_131}),
        .DOUTPADOUTP(NLW_RAM_reg_bram_22_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_22_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_22_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_22_i_1_n_0),
        .ENBWREN(RAM_reg_bram_22_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_22_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_22_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_22_i_3_n_0,RAM_reg_bram_22_i_3_n_0,RAM_reg_bram_22_i_3_n_0,RAM_reg_bram_22_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_i_4_n_0,RAM_reg_bram_22_i_4_n_0,RAM_reg_bram_22_i_4_n_0,RAM_reg_bram_22_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_22_i_1
       (.I0(i_en),
        .I1(i_addr[16]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_22_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_22_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[16]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_22_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_22_i_3
       (.I0(i_wen),
        .I1(i_addr[16]),
        .I2(i_addr[12]),
        .I3(i_addr[15]),
        .I4(i_addr[14]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_22_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_22_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[16]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[15]),
        .I4(RAM_reg_mux_sel_reg_3_0[14]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_22_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "4095" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "0" *) 
  (* ram_addr_end = "4095" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("FIRST"),
    .CASCADE_ORDER_B("FIRST"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_4
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .CASDINPA({1'b0,1'b0,1'b0,1'b0}),
        .CASDINPB({1'b0,1'b0,1'b0,1'b0}),
        .CASDOMUXA(1'b0),
        .CASDOMUXB(1'b0),
        .CASDOMUXEN_A(1'b1),
        .CASDOMUXEN_B(1'b1),
        .CASDOUTA({NLW_RAM_reg_bram_4_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_4_n_28,RAM_reg_bram_4_n_29,RAM_reg_bram_4_n_30,RAM_reg_bram_4_n_31,RAM_reg_bram_4_n_32,RAM_reg_bram_4_n_33,RAM_reg_bram_4_n_34,RAM_reg_bram_4_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_4_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_4_n_60,RAM_reg_bram_4_n_61,RAM_reg_bram_4_n_62,RAM_reg_bram_4_n_63,RAM_reg_bram_4_n_64,RAM_reg_bram_4_n_65,RAM_reg_bram_4_n_66,RAM_reg_bram_4_n_67}),
        .CASDOUTPA({RAM_reg_bram_4_n_132,RAM_reg_bram_4_n_133,RAM_reg_bram_4_n_134,RAM_reg_bram_4_n_135}),
        .CASDOUTPB({RAM_reg_bram_4_n_136,RAM_reg_bram_4_n_137,RAM_reg_bram_4_n_138,RAM_reg_bram_4_n_139}),
        .CASINDBITERR(1'b0),
        .CASINSBITERR(1'b0),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_4_n_0),
        .CASOUTSBITERR(RAM_reg_bram_4_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_4_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_4_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_4_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_4_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_4_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_4_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_4_i_1_n_0),
        .ENBWREN(RAM_reg_bram_4_i_2_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_4_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_4_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_4_i_3_n_0,RAM_reg_bram_4_i_3_n_0,RAM_reg_bram_4_i_3_n_0,RAM_reg_bram_4_i_3_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_4_i_4_n_0,RAM_reg_bram_4_i_4_n_0,RAM_reg_bram_4_i_4_n_0,RAM_reg_bram_4_i_4_n_0}));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    RAM_reg_bram_4_i_1
       (.I0(i_en),
        .I1(i_addr[12]),
        .I2(i_addr[14]),
        .I3(i_addr[13]),
        .I4(i_addr[16]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_4_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    RAM_reg_bram_4_i_2
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[14]),
        .I3(RAM_reg_mux_sel_reg_3_0[13]),
        .I4(RAM_reg_mux_sel_reg_3_0[16]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_4_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    RAM_reg_bram_4_i_3
       (.I0(i_wen),
        .I1(i_addr[12]),
        .I2(i_addr[14]),
        .I3(i_addr[13]),
        .I4(i_addr[16]),
        .I5(i_addr[15]),
        .O(RAM_reg_bram_4_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    RAM_reg_bram_4_i_4
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[14]),
        .I3(RAM_reg_mux_sel_reg_3_0[13]),
        .I4(RAM_reg_mux_sel_reg_3_0[16]),
        .I5(RAM_reg_mux_sel_reg_3_0[15]),
        .O(RAM_reg_bram_4_i_4_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "4096" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "4096" *) 
  (* ram_addr_end = "8191" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_5
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_4_n_28,RAM_reg_bram_4_n_29,RAM_reg_bram_4_n_30,RAM_reg_bram_4_n_31,RAM_reg_bram_4_n_32,RAM_reg_bram_4_n_33,RAM_reg_bram_4_n_34,RAM_reg_bram_4_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_4_n_60,RAM_reg_bram_4_n_61,RAM_reg_bram_4_n_62,RAM_reg_bram_4_n_63,RAM_reg_bram_4_n_64,RAM_reg_bram_4_n_65,RAM_reg_bram_4_n_66,RAM_reg_bram_4_n_67}),
        .CASDINPA({RAM_reg_bram_4_n_132,RAM_reg_bram_4_n_133,RAM_reg_bram_4_n_134,RAM_reg_bram_4_n_135}),
        .CASDINPB({RAM_reg_bram_4_n_136,RAM_reg_bram_4_n_137,RAM_reg_bram_4_n_138,RAM_reg_bram_4_n_139}),
        .CASDOMUXA(RAM_reg_bram_5_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_5_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_5_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_5_n_28,RAM_reg_bram_5_n_29,RAM_reg_bram_5_n_30,RAM_reg_bram_5_n_31,RAM_reg_bram_5_n_32,RAM_reg_bram_5_n_33,RAM_reg_bram_5_n_34,RAM_reg_bram_5_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_5_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_5_n_60,RAM_reg_bram_5_n_61,RAM_reg_bram_5_n_62,RAM_reg_bram_5_n_63,RAM_reg_bram_5_n_64,RAM_reg_bram_5_n_65,RAM_reg_bram_5_n_66,RAM_reg_bram_5_n_67}),
        .CASDOUTPA({RAM_reg_bram_5_n_132,RAM_reg_bram_5_n_133,RAM_reg_bram_5_n_134,RAM_reg_bram_5_n_135}),
        .CASDOUTPB({RAM_reg_bram_5_n_136,RAM_reg_bram_5_n_137,RAM_reg_bram_5_n_138,RAM_reg_bram_5_n_139}),
        .CASINDBITERR(RAM_reg_bram_4_n_0),
        .CASINSBITERR(RAM_reg_bram_4_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_5_n_0),
        .CASOUTSBITERR(RAM_reg_bram_5_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_5_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_5_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_5_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_5_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_5_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_5_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_5_i_3_n_0),
        .ENBWREN(RAM_reg_bram_5_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_5_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_5_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_5_i_5_n_0,RAM_reg_bram_5_i_5_n_0,RAM_reg_bram_5_i_5_n_0,RAM_reg_bram_5_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_5_i_6_n_0,RAM_reg_bram_5_i_6_n_0,RAM_reg_bram_5_i_6_n_0,RAM_reg_bram_5_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    RAM_reg_bram_5_i_1
       (.I0(i_addr[12]),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .O(RAM_reg_bram_5_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    RAM_reg_bram_5_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[12]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_5_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_5_i_3
       (.I0(i_en),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_5_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_5_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_5_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_5_i_5
       (.I0(i_wen),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_5_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_5_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_5_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "8192" *) 
  (* bram_addr_end = "12287" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "8192" *) 
  (* ram_addr_end = "12287" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_6
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_5_n_28,RAM_reg_bram_5_n_29,RAM_reg_bram_5_n_30,RAM_reg_bram_5_n_31,RAM_reg_bram_5_n_32,RAM_reg_bram_5_n_33,RAM_reg_bram_5_n_34,RAM_reg_bram_5_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_5_n_60,RAM_reg_bram_5_n_61,RAM_reg_bram_5_n_62,RAM_reg_bram_5_n_63,RAM_reg_bram_5_n_64,RAM_reg_bram_5_n_65,RAM_reg_bram_5_n_66,RAM_reg_bram_5_n_67}),
        .CASDINPA({RAM_reg_bram_5_n_132,RAM_reg_bram_5_n_133,RAM_reg_bram_5_n_134,RAM_reg_bram_5_n_135}),
        .CASDINPB({RAM_reg_bram_5_n_136,RAM_reg_bram_5_n_137,RAM_reg_bram_5_n_138,RAM_reg_bram_5_n_139}),
        .CASDOMUXA(RAM_reg_bram_6_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_6_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_6_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_6_n_28,RAM_reg_bram_6_n_29,RAM_reg_bram_6_n_30,RAM_reg_bram_6_n_31,RAM_reg_bram_6_n_32,RAM_reg_bram_6_n_33,RAM_reg_bram_6_n_34,RAM_reg_bram_6_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_6_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_6_n_60,RAM_reg_bram_6_n_61,RAM_reg_bram_6_n_62,RAM_reg_bram_6_n_63,RAM_reg_bram_6_n_64,RAM_reg_bram_6_n_65,RAM_reg_bram_6_n_66,RAM_reg_bram_6_n_67}),
        .CASDOUTPA({RAM_reg_bram_6_n_132,RAM_reg_bram_6_n_133,RAM_reg_bram_6_n_134,RAM_reg_bram_6_n_135}),
        .CASDOUTPB({RAM_reg_bram_6_n_136,RAM_reg_bram_6_n_137,RAM_reg_bram_6_n_138,RAM_reg_bram_6_n_139}),
        .CASINDBITERR(RAM_reg_bram_5_n_0),
        .CASINSBITERR(RAM_reg_bram_5_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_6_n_0),
        .CASOUTSBITERR(RAM_reg_bram_6_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_6_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_6_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_6_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_6_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_6_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_6_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_6_i_3_n_0),
        .ENBWREN(RAM_reg_bram_6_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_6_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_6_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_6_i_5_n_0,RAM_reg_bram_6_i_5_n_0,RAM_reg_bram_6_i_5_n_0,RAM_reg_bram_6_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_6_i_6_n_0,RAM_reg_bram_6_i_6_n_0,RAM_reg_bram_6_i_6_n_0,RAM_reg_bram_6_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    RAM_reg_bram_6_i_1
       (.I0(i_addr[13]),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .O(RAM_reg_bram_6_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hFD)) 
    RAM_reg_bram_6_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[13]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_6_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_6_i_3
       (.I0(i_en),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_6_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_6_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_6_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_6_i_5
       (.I0(i_wen),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[13]),
        .O(RAM_reg_bram_6_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_6_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[13]),
        .O(RAM_reg_bram_6_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "12288" *) 
  (* bram_addr_end = "16383" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "12288" *) 
  (* ram_addr_end = "16383" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_7
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_6_n_28,RAM_reg_bram_6_n_29,RAM_reg_bram_6_n_30,RAM_reg_bram_6_n_31,RAM_reg_bram_6_n_32,RAM_reg_bram_6_n_33,RAM_reg_bram_6_n_34,RAM_reg_bram_6_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_6_n_60,RAM_reg_bram_6_n_61,RAM_reg_bram_6_n_62,RAM_reg_bram_6_n_63,RAM_reg_bram_6_n_64,RAM_reg_bram_6_n_65,RAM_reg_bram_6_n_66,RAM_reg_bram_6_n_67}),
        .CASDINPA({RAM_reg_bram_6_n_132,RAM_reg_bram_6_n_133,RAM_reg_bram_6_n_134,RAM_reg_bram_6_n_135}),
        .CASDINPB({RAM_reg_bram_6_n_136,RAM_reg_bram_6_n_137,RAM_reg_bram_6_n_138,RAM_reg_bram_6_n_139}),
        .CASDOMUXA(RAM_reg_bram_7_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_7_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_7_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_7_n_28,RAM_reg_bram_7_n_29,RAM_reg_bram_7_n_30,RAM_reg_bram_7_n_31,RAM_reg_bram_7_n_32,RAM_reg_bram_7_n_33,RAM_reg_bram_7_n_34,RAM_reg_bram_7_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_7_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_7_n_60,RAM_reg_bram_7_n_61,RAM_reg_bram_7_n_62,RAM_reg_bram_7_n_63,RAM_reg_bram_7_n_64,RAM_reg_bram_7_n_65,RAM_reg_bram_7_n_66,RAM_reg_bram_7_n_67}),
        .CASDOUTPA({RAM_reg_bram_7_n_132,RAM_reg_bram_7_n_133,RAM_reg_bram_7_n_134,RAM_reg_bram_7_n_135}),
        .CASDOUTPB({RAM_reg_bram_7_n_136,RAM_reg_bram_7_n_137,RAM_reg_bram_7_n_138,RAM_reg_bram_7_n_139}),
        .CASINDBITERR(RAM_reg_bram_6_n_0),
        .CASINSBITERR(RAM_reg_bram_6_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_7_n_0),
        .CASOUTSBITERR(RAM_reg_bram_7_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_7_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_7_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_7_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_7_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_7_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_7_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_7_i_3_n_0),
        .ENBWREN(RAM_reg_bram_7_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_7_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_7_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_7_i_5_n_0,RAM_reg_bram_7_i_5_n_0,RAM_reg_bram_7_i_5_n_0,RAM_reg_bram_7_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_7_i_6_n_0,RAM_reg_bram_7_i_6_n_0,RAM_reg_bram_7_i_6_n_0,RAM_reg_bram_7_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_7_i_1
       (.I0(i_addr[14]),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .O(RAM_reg_bram_7_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_7_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[14]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_7_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_7_i_3
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[14]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_7_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_7_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[14]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_7_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_7_i_5
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[14]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_7_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_7_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[14]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_7_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "16384" *) 
  (* bram_addr_end = "20479" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "16384" *) 
  (* ram_addr_end = "20479" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_8
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_7_n_28,RAM_reg_bram_7_n_29,RAM_reg_bram_7_n_30,RAM_reg_bram_7_n_31,RAM_reg_bram_7_n_32,RAM_reg_bram_7_n_33,RAM_reg_bram_7_n_34,RAM_reg_bram_7_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_7_n_60,RAM_reg_bram_7_n_61,RAM_reg_bram_7_n_62,RAM_reg_bram_7_n_63,RAM_reg_bram_7_n_64,RAM_reg_bram_7_n_65,RAM_reg_bram_7_n_66,RAM_reg_bram_7_n_67}),
        .CASDINPA({RAM_reg_bram_7_n_132,RAM_reg_bram_7_n_133,RAM_reg_bram_7_n_134,RAM_reg_bram_7_n_135}),
        .CASDINPB({RAM_reg_bram_7_n_136,RAM_reg_bram_7_n_137,RAM_reg_bram_7_n_138,RAM_reg_bram_7_n_139}),
        .CASDOMUXA(RAM_reg_bram_8_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_8_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_8_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_8_n_28,RAM_reg_bram_8_n_29,RAM_reg_bram_8_n_30,RAM_reg_bram_8_n_31,RAM_reg_bram_8_n_32,RAM_reg_bram_8_n_33,RAM_reg_bram_8_n_34,RAM_reg_bram_8_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_8_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_8_n_60,RAM_reg_bram_8_n_61,RAM_reg_bram_8_n_62,RAM_reg_bram_8_n_63,RAM_reg_bram_8_n_64,RAM_reg_bram_8_n_65,RAM_reg_bram_8_n_66,RAM_reg_bram_8_n_67}),
        .CASDOUTPA({RAM_reg_bram_8_n_132,RAM_reg_bram_8_n_133,RAM_reg_bram_8_n_134,RAM_reg_bram_8_n_135}),
        .CASDOUTPB({RAM_reg_bram_8_n_136,RAM_reg_bram_8_n_137,RAM_reg_bram_8_n_138,RAM_reg_bram_8_n_139}),
        .CASINDBITERR(RAM_reg_bram_7_n_0),
        .CASINSBITERR(RAM_reg_bram_7_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_8_n_0),
        .CASOUTSBITERR(RAM_reg_bram_8_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_8_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_8_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_8_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_8_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_8_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_8_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_8_i_3_n_0),
        .ENBWREN(RAM_reg_bram_8_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_8_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_8_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_8_i_5_n_0,RAM_reg_bram_8_i_5_n_0,RAM_reg_bram_8_i_5_n_0,RAM_reg_bram_8_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_8_i_6_n_0,RAM_reg_bram_8_i_6_n_0,RAM_reg_bram_8_i_6_n_0,RAM_reg_bram_8_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    RAM_reg_bram_8_i_1
       (.I0(i_addr[13]),
        .I1(i_addr[12]),
        .I2(i_addr[14]),
        .O(RAM_reg_bram_8_i_1_n_0));
  LUT3 #(
    .INIT(8'hEF)) 
    RAM_reg_bram_8_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[13]),
        .I1(RAM_reg_mux_sel_reg_3_0[12]),
        .I2(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_8_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_8_i_3
       (.I0(i_en),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_8_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_8_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_8_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_8_i_5
       (.I0(i_wen),
        .I1(i_addr[13]),
        .I2(i_addr[12]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[14]),
        .O(RAM_reg_bram_8_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    RAM_reg_bram_8_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[13]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[14]),
        .O(RAM_reg_bram_8_i_6_n_0));
  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d8" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-7 {cell *THIS*}}" *) 
  (* RDADDR_COLLISION_HWCONFIG = "DELAYED_WRITE" *) 
  (* RTL_RAM_BITS = "614400" *) 
  (* RTL_RAM_NAME = "RAM" *) 
  (* bram_addr_begin = "20480" *) 
  (* bram_addr_end = "24575" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "7" *) 
  (* ram_addr_begin = "20480" *) 
  (* ram_addr_end = "24575" *) 
  (* ram_offset = "0" *) 
  (* ram_slice_begin = "0" *) 
  (* ram_slice_end = "7" *) 
  RAMB36E2 #(
    .CASCADE_ORDER_A("MIDDLE"),
    .CASCADE_ORDER_B("MIDDLE"),
    .CLOCK_DOMAINS("INDEPENDENT"),
    .DOA_REG(0),
    .DOB_REG(0),
    .ENADDRENA("FALSE"),
    .ENADDRENB("FALSE"),
    .EN_ECC_PIPE("FALSE"),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .INIT_FILE("NONE"),
    .RDADDRCHANGEA("FALSE"),
    .RDADDRCHANGEB("FALSE"),
    .READ_WIDTH_A(9),
    .READ_WIDTH_B(9),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SLEEP_ASYNC("FALSE"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("READ_FIRST"),
    .WRITE_WIDTH_A(9),
    .WRITE_WIDTH_B(9)) 
    RAM_reg_bram_9
       (.ADDRARDADDR({i_addr[11:0],1'b1,1'b1,1'b1}),
        .ADDRBWRADDR({RAM_reg_mux_sel_reg_3_0[11:0],1'b1,1'b1,1'b1}),
        .ADDRENA(1'b0),
        .ADDRENB(1'b0),
        .CASDIMUXA(1'b0),
        .CASDIMUXB(1'b0),
        .CASDINA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_8_n_28,RAM_reg_bram_8_n_29,RAM_reg_bram_8_n_30,RAM_reg_bram_8_n_31,RAM_reg_bram_8_n_32,RAM_reg_bram_8_n_33,RAM_reg_bram_8_n_34,RAM_reg_bram_8_n_35}),
        .CASDINB({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_8_n_60,RAM_reg_bram_8_n_61,RAM_reg_bram_8_n_62,RAM_reg_bram_8_n_63,RAM_reg_bram_8_n_64,RAM_reg_bram_8_n_65,RAM_reg_bram_8_n_66,RAM_reg_bram_8_n_67}),
        .CASDINPA({RAM_reg_bram_8_n_132,RAM_reg_bram_8_n_133,RAM_reg_bram_8_n_134,RAM_reg_bram_8_n_135}),
        .CASDINPB({RAM_reg_bram_8_n_136,RAM_reg_bram_8_n_137,RAM_reg_bram_8_n_138,RAM_reg_bram_8_n_139}),
        .CASDOMUXA(RAM_reg_bram_9_i_1_n_0),
        .CASDOMUXB(RAM_reg_bram_9_i_2_n_0),
        .CASDOMUXEN_A(i_en),
        .CASDOMUXEN_B(Q[0]),
        .CASDOUTA({NLW_RAM_reg_bram_9_CASDOUTA_UNCONNECTED[31:8],RAM_reg_bram_9_n_28,RAM_reg_bram_9_n_29,RAM_reg_bram_9_n_30,RAM_reg_bram_9_n_31,RAM_reg_bram_9_n_32,RAM_reg_bram_9_n_33,RAM_reg_bram_9_n_34,RAM_reg_bram_9_n_35}),
        .CASDOUTB({NLW_RAM_reg_bram_9_CASDOUTB_UNCONNECTED[31:8],RAM_reg_bram_9_n_60,RAM_reg_bram_9_n_61,RAM_reg_bram_9_n_62,RAM_reg_bram_9_n_63,RAM_reg_bram_9_n_64,RAM_reg_bram_9_n_65,RAM_reg_bram_9_n_66,RAM_reg_bram_9_n_67}),
        .CASDOUTPA({RAM_reg_bram_9_n_132,RAM_reg_bram_9_n_133,RAM_reg_bram_9_n_134,RAM_reg_bram_9_n_135}),
        .CASDOUTPB({RAM_reg_bram_9_n_136,RAM_reg_bram_9_n_137,RAM_reg_bram_9_n_138,RAM_reg_bram_9_n_139}),
        .CASINDBITERR(RAM_reg_bram_8_n_0),
        .CASINSBITERR(RAM_reg_bram_8_n_1),
        .CASOREGIMUXA(1'b0),
        .CASOREGIMUXB(1'b0),
        .CASOREGIMUXEN_A(1'b1),
        .CASOREGIMUXEN_B(1'b1),
        .CASOUTDBITERR(RAM_reg_bram_9_n_0),
        .CASOUTSBITERR(RAM_reg_bram_9_n_1),
        .CLKARDCLK(s_clk),
        .CLKBWRCLK(s00_axi_aclk),
        .DBITERR(NLW_RAM_reg_bram_9_DBITERR_UNCONNECTED),
        .DINADIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_px}),
        .DINBDIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_22_0}),
        .DINPADINP({1'b0,1'b0,1'b0,1'b0}),
        .DINPBDINP({1'b0,1'b0,1'b0,1'b0}),
        .DOUTADOUT(NLW_RAM_reg_bram_9_DOUTADOUT_UNCONNECTED[31:0]),
        .DOUTBDOUT(NLW_RAM_reg_bram_9_DOUTBDOUT_UNCONNECTED[31:0]),
        .DOUTPADOUTP(NLW_RAM_reg_bram_9_DOUTPADOUTP_UNCONNECTED[3:0]),
        .DOUTPBDOUTP(NLW_RAM_reg_bram_9_DOUTPBDOUTP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_RAM_reg_bram_9_ECCPARITY_UNCONNECTED[7:0]),
        .ECCPIPECE(1'b1),
        .ENARDEN(RAM_reg_bram_9_i_3_n_0),
        .ENBWREN(RAM_reg_bram_9_i_4_n_0),
        .INJECTDBITERR(1'b0),
        .INJECTSBITERR(1'b0),
        .RDADDRECC(NLW_RAM_reg_bram_9_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b1),
        .REGCEB(1'b1),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_RAM_reg_bram_9_SBITERR_UNCONNECTED),
        .SLEEP(1'b0),
        .WEA({RAM_reg_bram_9_i_5_n_0,RAM_reg_bram_9_i_5_n_0,RAM_reg_bram_9_i_5_n_0,RAM_reg_bram_9_i_5_n_0}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,RAM_reg_bram_9_i_6_n_0,RAM_reg_bram_9_i_6_n_0,RAM_reg_bram_9_i_6_n_0,RAM_reg_bram_9_i_6_n_0}));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_9_i_1
       (.I0(i_addr[13]),
        .I1(i_addr[14]),
        .I2(i_addr[12]),
        .O(RAM_reg_bram_9_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    RAM_reg_bram_9_i_2
       (.I0(RAM_reg_mux_sel_reg_3_0[13]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_9_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_9_i_3
       (.I0(i_en),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_9_i_3_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_9_i_4
       (.I0(Q[0]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_9_i_4_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_9_i_5
       (.I0(i_wen),
        .I1(i_addr[14]),
        .I2(i_addr[13]),
        .I3(i_addr[16]),
        .I4(i_addr[15]),
        .I5(i_addr[12]),
        .O(RAM_reg_bram_9_i_5_n_0));
  LUT6 #(
    .INIT(64'h0000000800000000)) 
    RAM_reg_bram_9_i_6
       (.I0(Q[1]),
        .I1(RAM_reg_mux_sel_reg_3_0[14]),
        .I2(RAM_reg_mux_sel_reg_3_0[13]),
        .I3(RAM_reg_mux_sel_reg_3_0[16]),
        .I4(RAM_reg_mux_sel_reg_3_0[15]),
        .I5(RAM_reg_mux_sel_reg_3_0[12]),
        .O(RAM_reg_bram_9_i_6_n_0));
  FDRE RAM_reg_mux_sel_reg_2
       (.C(s00_axi_aclk),
        .CE(Q[0]),
        .D(RAM_reg_mux_sel_reg_3_0[15]),
        .Q(RAM_reg_mux_sel_reg_2_n_0),
        .R(1'b0));
  FDRE RAM_reg_mux_sel_reg_3
       (.C(s00_axi_aclk),
        .CE(Q[0]),
        .D(RAM_reg_mux_sel_reg_3_0[16]),
        .Q(RAM_reg_mux_sel_reg_3_n_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[0]_i_5 
       (.I0(RAM_reg_bram_22_n_131),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_131),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_131),
        .O(dob[0]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[1]_i_5 
       (.I0(RAM_reg_bram_22_n_130),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_130),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_130),
        .O(dob[1]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[2]_i_5 
       (.I0(RAM_reg_bram_22_n_129),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_129),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_129),
        .O(dob[2]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[3]_i_5 
       (.I0(RAM_reg_bram_22_n_128),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_128),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_128),
        .O(dob[3]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[4]_i_5 
       (.I0(RAM_reg_bram_22_n_127),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_127),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_127),
        .O(dob[4]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[5]_i_5 
       (.I0(RAM_reg_bram_22_n_126),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_126),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_126),
        .O(dob[5]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[6]_i_5 
       (.I0(RAM_reg_bram_22_n_125),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_125),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_125),
        .O(dob[6]));
  LUT5 #(
    .INIT(32'h30BB3088)) 
    \axi_rdata[7]_i_6 
       (.I0(RAM_reg_bram_22_n_124),
        .I1(RAM_reg_mux_sel_reg_3_n_0),
        .I2(RAM_reg_bram_19_n_124),
        .I3(RAM_reg_mux_sel_reg_2_n_0),
        .I4(RAM_reg_bram_11_n_124),
        .O(dob[7]));
  FDRE doa_ok_reg
       (.C(s_clk),
        .CE(1'b1),
        .D(i_en),
        .Q(o_mf),
        .R(1'b0));
  FDRE dob_ok_reg
       (.C(s00_axi_aclk),
        .CE(1'b1),
        .D(Q[0]),
        .Q(data0),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
