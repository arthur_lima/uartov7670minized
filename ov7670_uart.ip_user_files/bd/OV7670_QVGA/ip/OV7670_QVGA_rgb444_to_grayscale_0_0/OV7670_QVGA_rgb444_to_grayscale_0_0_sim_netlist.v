// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
// Date        : Fri Sep 11 15:25:02 2020
// Host        : devlaptop running 64-bit Ubuntu 18.04.4 LTS
// Command     : write_verilog -force -mode funcsim
//               /home/bsilva/Documentos/UnB/Final-Project/digital_systems/ov7670mcs/ov7670minized_vga/zcu_cmos/zcu_cmos.srcs/sources_1/bd/OV7670_QVGA/ip/OV7670_QVGA_rgb444_to_grayscale_0_0/OV7670_QVGA_rgb444_to_grayscale_0_0_sim_netlist.v
// Design      : OV7670_QVGA_rgb444_to_grayscale_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xczu7ev-ffvc1156-2-e
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "OV7670_QVGA_rgb444_to_grayscale_0_0,rgb444_to_grayscale,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* ip_definition_source = "module_ref" *) 
(* x_core_info = "rgb444_to_grayscale,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module OV7670_QVGA_rgb444_to_grayscale_0_0
   (i_rgb,
    o_gray);
  input [11:0]i_rgb;
  output [7:0]o_gray;

  wire [11:0]i_rgb;
  wire [7:0]o_gray;
  wire \o_gray[1]_INST_0_i_1_n_0 ;
  wire \o_gray[2]_INST_0_i_3_n_0 ;
  wire \o_gray[5]_INST_0_i_5_n_0 ;

  OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale U0
       (.i_rgb(i_rgb),
        .o_gray(o_gray),
        .o_gray_1_sp_1(\o_gray[1]_INST_0_i_1_n_0 ),
        .o_gray_2_sp_1(\o_gray[2]_INST_0_i_3_n_0 ),
        .o_gray_5_sp_1(\o_gray[5]_INST_0_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFF01)) 
    \o_gray[1]_INST_0_i_1 
       (.I0(i_rgb[1]),
        .I1(i_rgb[3]),
        .I2(i_rgb[0]),
        .I3(i_rgb[2]),
        .O(\o_gray[1]_INST_0_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0FF1)) 
    \o_gray[2]_INST_0_i_3 
       (.I0(i_rgb[2]),
        .I1(i_rgb[1]),
        .I2(i_rgb[3]),
        .I3(i_rgb[0]),
        .O(\o_gray[2]_INST_0_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hF30D)) 
    \o_gray[5]_INST_0_i_5 
       (.I0(i_rgb[2]),
        .I1(i_rgb[0]),
        .I2(i_rgb[3]),
        .I3(i_rgb[1]),
        .O(\o_gray[5]_INST_0_i_5_n_0 ));
endmodule

(* ORIG_REF_NAME = "rgb444_to_grayscale" *) 
module OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale
   (o_gray,
    i_rgb,
    o_gray_5_sp_1,
    o_gray_2_sp_1,
    o_gray_1_sp_1);
  output [7:0]o_gray;
  input [11:0]i_rgb;
  input o_gray_5_sp_1;
  input o_gray_2_sp_1;
  input o_gray_1_sp_1;

  wire [7:0]final_g;
  wire [11:0]i_rgb;
  wire mult_g__0_carry__0_i_10_n_0;
  wire mult_g__0_carry__0_i_11_n_0;
  wire mult_g__0_carry__0_i_1_n_0;
  wire mult_g__0_carry__0_i_2_n_0;
  wire mult_g__0_carry__0_i_3_n_0;
  wire mult_g__0_carry__0_i_4_n_0;
  wire mult_g__0_carry__0_i_5_n_0;
  wire mult_g__0_carry__0_i_6_n_0;
  wire mult_g__0_carry__0_i_7_n_0;
  wire mult_g__0_carry__0_i_8_n_0;
  wire mult_g__0_carry__0_i_9_n_0;
  wire mult_g__0_carry__0_n_0;
  wire mult_g__0_carry__0_n_1;
  wire mult_g__0_carry__0_n_2;
  wire mult_g__0_carry__0_n_3;
  wire mult_g__0_carry__0_n_4;
  wire mult_g__0_carry__0_n_5;
  wire mult_g__0_carry__0_n_6;
  wire mult_g__0_carry__0_n_7;
  wire mult_g__0_carry__1_i_1_n_0;
  wire mult_g__0_carry__1_i_2_n_0;
  wire mult_g__0_carry__1_n_7;
  wire mult_g__0_carry_i_1_n_0;
  wire mult_g__0_carry_i_2_n_0;
  wire mult_g__0_carry_i_3_n_0;
  wire mult_g__0_carry_i_4_n_0;
  wire mult_g__0_carry_i_5_n_0;
  wire mult_g__0_carry_i_6_n_0;
  wire mult_g__0_carry_i_7_n_0;
  wire mult_g__0_carry_i_8_n_0;
  wire mult_g__0_carry_i_9_n_0;
  wire mult_g__0_carry_n_0;
  wire mult_g__0_carry_n_1;
  wire mult_g__0_carry_n_2;
  wire mult_g__0_carry_n_3;
  wire mult_g__0_carry_n_4;
  wire mult_g__0_carry_n_5;
  wire mult_g__0_carry_n_6;
  wire mult_g__0_carry_n_7;
  wire [16:10]mult_r;
  wire [7:0]o_gray;
  wire \o_gray[1]_INST_0_i_2_n_0 ;
  wire \o_gray[2]_INST_0_i_1_n_0 ;
  wire \o_gray[2]_INST_0_i_2_n_0 ;
  wire \o_gray[3]_INST_0_i_1_n_0 ;
  wire \o_gray[3]_INST_0_i_2_n_0 ;
  wire \o_gray[3]_INST_0_i_3_n_0 ;
  wire \o_gray[5]_INST_0_i_1_n_0 ;
  wire \o_gray[5]_INST_0_i_2_n_0 ;
  wire \o_gray[5]_INST_0_i_3_n_0 ;
  wire \o_gray[5]_INST_0_i_4_n_0 ;
  wire \o_gray[5]_INST_0_i_6_n_0 ;
  wire \o_gray[5]_INST_0_i_7_n_0 ;
  wire \o_gray[7]_INST_0_i_1_n_0 ;
  wire \o_gray[7]_INST_0_i_2_n_3 ;
  wire \o_gray[7]_INST_0_i_2_n_4 ;
  wire \o_gray[7]_INST_0_i_2_n_5 ;
  wire \o_gray[7]_INST_0_i_2_n_6 ;
  wire \o_gray[7]_INST_0_i_2_n_7 ;
  wire \o_gray[7]_INST_0_i_3_n_0 ;
  wire \o_gray[7]_INST_0_i_4_n_0 ;
  wire \o_gray[7]_INST_0_i_5_n_0 ;
  wire o_gray_1_sn_1;
  wire o_gray_2_sn_1;
  wire o_gray_5_sn_1;
  wire [7:0]NLW_mult_g__0_carry_O_UNCONNECTED;
  wire [1:0]NLW_mult_g__0_carry__0_O_UNCONNECTED;
  wire [7:1]NLW_mult_g__0_carry__1_CO_UNCONNECTED;
  wire [7:2]NLW_mult_g__0_carry__1_O_UNCONNECTED;
  wire [7:5]\NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED ;
  wire [7:0]\NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED ;

  assign o_gray_1_sn_1 = o_gray_1_sp_1;
  assign o_gray_2_sn_1 = o_gray_2_sp_1;
  assign o_gray_5_sn_1 = o_gray_5_sp_1;
  CARRY8 mult_g__0_carry
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({mult_g__0_carry_n_0,mult_g__0_carry_n_1,mult_g__0_carry_n_2,mult_g__0_carry_n_3,mult_g__0_carry_n_4,mult_g__0_carry_n_5,mult_g__0_carry_n_6,mult_g__0_carry_n_7}),
        .DI({i_rgb[6],mult_g__0_carry_i_1_n_0,mult_g__0_carry_i_2_n_0,i_rgb[5],i_rgb[7],1'b0,1'b0,1'b1}),
        .O(NLW_mult_g__0_carry_O_UNCONNECTED[7:0]),
        .S({mult_g__0_carry_i_3_n_0,mult_g__0_carry_i_4_n_0,mult_g__0_carry_i_5_n_0,mult_g__0_carry_i_6_n_0,mult_g__0_carry_i_7_n_0,mult_g__0_carry_i_8_n_0,mult_g__0_carry_i_9_n_0,i_rgb[4]}));
  CARRY8 mult_g__0_carry__0
       (.CI(mult_g__0_carry_n_0),
        .CI_TOP(1'b0),
        .CO({mult_g__0_carry__0_n_0,mult_g__0_carry__0_n_1,mult_g__0_carry__0_n_2,mult_g__0_carry__0_n_3,mult_g__0_carry__0_n_4,mult_g__0_carry__0_n_5,mult_g__0_carry__0_n_6,mult_g__0_carry__0_n_7}),
        .DI({mult_g__0_carry__0_i_1_n_0,mult_g__0_carry__0_i_2_n_0,i_rgb[5],i_rgb[7],i_rgb[4],i_rgb[6:5],mult_g__0_carry__0_i_3_n_0}),
        .O({final_g[5:0],NLW_mult_g__0_carry__0_O_UNCONNECTED[1:0]}),
        .S({mult_g__0_carry__0_i_4_n_0,mult_g__0_carry__0_i_5_n_0,mult_g__0_carry__0_i_6_n_0,mult_g__0_carry__0_i_7_n_0,mult_g__0_carry__0_i_8_n_0,mult_g__0_carry__0_i_9_n_0,mult_g__0_carry__0_i_10_n_0,mult_g__0_carry__0_i_11_n_0}));
  LUT2 #(
    .INIT(4'hE)) 
    mult_g__0_carry__0_i_1
       (.I0(i_rgb[5]),
        .I1(i_rgb[7]),
        .O(mult_g__0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h2D)) 
    mult_g__0_carry__0_i_10
       (.I0(i_rgb[7]),
        .I1(i_rgb[4]),
        .I2(i_rgb[5]),
        .O(mult_g__0_carry__0_i_10_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    mult_g__0_carry__0_i_11
       (.I0(i_rgb[7]),
        .I1(i_rgb[4]),
        .I2(i_rgb[6]),
        .O(mult_g__0_carry__0_i_11_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    mult_g__0_carry__0_i_2
       (.I0(i_rgb[4]),
        .I1(i_rgb[6]),
        .O(mult_g__0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    mult_g__0_carry__0_i_3
       (.I0(i_rgb[4]),
        .I1(i_rgb[7]),
        .O(mult_g__0_carry__0_i_3_n_0));
  LUT3 #(
    .INIT(8'hE1)) 
    mult_g__0_carry__0_i_4
       (.I0(i_rgb[7]),
        .I1(i_rgb[5]),
        .I2(i_rgb[6]),
        .O(mult_g__0_carry__0_i_4_n_0));
  LUT4 #(
    .INIT(16'h1EE1)) 
    mult_g__0_carry__0_i_5
       (.I0(i_rgb[6]),
        .I1(i_rgb[4]),
        .I2(i_rgb[7]),
        .I3(i_rgb[5]),
        .O(mult_g__0_carry__0_i_5_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    mult_g__0_carry__0_i_6
       (.I0(i_rgb[6]),
        .I1(i_rgb[4]),
        .I2(i_rgb[5]),
        .O(mult_g__0_carry__0_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    mult_g__0_carry__0_i_7
       (.I0(i_rgb[7]),
        .I1(i_rgb[5]),
        .O(mult_g__0_carry__0_i_7_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    mult_g__0_carry__0_i_8
       (.I0(i_rgb[7]),
        .I1(i_rgb[4]),
        .O(mult_g__0_carry__0_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    mult_g__0_carry__0_i_9
       (.I0(i_rgb[6]),
        .O(mult_g__0_carry__0_i_9_n_0));
  CARRY8 mult_g__0_carry__1
       (.CI(mult_g__0_carry__0_n_0),
        .CI_TOP(1'b0),
        .CO({NLW_mult_g__0_carry__1_CO_UNCONNECTED[7:1],mult_g__0_carry__1_n_7}),
        .DI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,i_rgb[6]}),
        .O({NLW_mult_g__0_carry__1_O_UNCONNECTED[7:2],final_g[7:6]}),
        .S({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,mult_g__0_carry__1_i_1_n_0,mult_g__0_carry__1_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    mult_g__0_carry__1_i_1
       (.I0(i_rgb[7]),
        .O(mult_g__0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    mult_g__0_carry__1_i_2
       (.I0(i_rgb[6]),
        .I1(i_rgb[7]),
        .O(mult_g__0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    mult_g__0_carry_i_1
       (.I0(i_rgb[6]),
        .I1(i_rgb[4]),
        .O(mult_g__0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    mult_g__0_carry_i_2
       (.I0(i_rgb[5]),
        .O(mult_g__0_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'hE1)) 
    mult_g__0_carry_i_3
       (.I0(i_rgb[7]),
        .I1(i_rgb[5]),
        .I2(i_rgb[6]),
        .O(mult_g__0_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h1EE1)) 
    mult_g__0_carry_i_4
       (.I0(i_rgb[6]),
        .I1(i_rgb[4]),
        .I2(i_rgb[7]),
        .I3(i_rgb[5]),
        .O(mult_g__0_carry_i_4_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    mult_g__0_carry_i_5
       (.I0(i_rgb[5]),
        .I1(i_rgb[6]),
        .I2(i_rgb[4]),
        .O(mult_g__0_carry_i_5_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    mult_g__0_carry_i_6
       (.I0(i_rgb[7]),
        .I1(i_rgb[5]),
        .O(mult_g__0_carry_i_6_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    mult_g__0_carry_i_7
       (.I0(i_rgb[7]),
        .I1(i_rgb[4]),
        .O(mult_g__0_carry_i_7_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    mult_g__0_carry_i_8
       (.I0(i_rgb[6]),
        .O(mult_g__0_carry_i_8_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    mult_g__0_carry_i_9
       (.I0(i_rgb[5]),
        .O(mult_g__0_carry_i_9_n_0));
  LUT6 #(
    .INIT(64'h6666666699999996)) 
    \o_gray[0]_INST_0 
       (.I0(mult_r[10]),
        .I1(final_g[0]),
        .I2(i_rgb[1]),
        .I3(i_rgb[3]),
        .I4(i_rgb[0]),
        .I5(i_rgb[2]),
        .O(o_gray[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \o_gray[0]_INST_0_i_1 
       (.I0(i_rgb[8]),
        .I1(i_rgb[11]),
        .O(mult_r[10]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h9F0960F6)) 
    \o_gray[1]_INST_0 
       (.I0(i_rgb[8]),
        .I1(i_rgb[11]),
        .I2(final_g[0]),
        .I3(o_gray_1_sn_1),
        .I4(\o_gray[1]_INST_0_i_2_n_0 ),
        .O(o_gray[1]));
  LUT6 #(
    .INIT(64'h9999666666669996)) 
    \o_gray[1]_INST_0_i_2 
       (.I0(mult_r[11]),
        .I1(final_g[1]),
        .I2(i_rgb[2]),
        .I3(i_rgb[1]),
        .I4(i_rgb[3]),
        .I5(i_rgb[0]),
        .O(\o_gray[1]_INST_0_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h396363C6)) 
    \o_gray[2]_INST_0 
       (.I0(\o_gray[2]_INST_0_i_1_n_0 ),
        .I1(\o_gray[2]_INST_0_i_2_n_0 ),
        .I2(o_gray_2_sn_1),
        .I3(final_g[1]),
        .I4(mult_r[11]),
        .O(o_gray[2]));
  LUT6 #(
    .INIT(64'hFFFF555455540000)) 
    \o_gray[2]_INST_0_i_1 
       (.I0(i_rgb[2]),
        .I1(i_rgb[0]),
        .I2(i_rgb[3]),
        .I3(i_rgb[1]),
        .I4(final_g[0]),
        .I5(mult_r[10]),
        .O(\o_gray[2]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6666996699996696)) 
    \o_gray[2]_INST_0_i_2 
       (.I0(mult_r[12]),
        .I1(final_g[2]),
        .I2(i_rgb[2]),
        .I3(i_rgb[0]),
        .I4(i_rgb[3]),
        .I5(i_rgb[1]),
        .O(\o_gray[2]_INST_0_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \o_gray[3]_INST_0 
       (.I0(\o_gray[3]_INST_0_i_1_n_0 ),
        .I1(\o_gray[3]_INST_0_i_2_n_0 ),
        .I2(\o_gray[3]_INST_0_i_3_n_0 ),
        .O(o_gray[3]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hA8EA80A8)) 
    \o_gray[3]_INST_0_i_1 
       (.I0(\o_gray[2]_INST_0_i_2_n_0 ),
        .I1(mult_r[11]),
        .I2(final_g[1]),
        .I3(o_gray_2_sn_1),
        .I4(\o_gray[2]_INST_0_i_1_n_0 ),
        .O(\o_gray[3]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h9699969669666966)) 
    \o_gray[3]_INST_0_i_2 
       (.I0(mult_r[13]),
        .I1(final_g[3]),
        .I2(i_rgb[1]),
        .I3(i_rgb[3]),
        .I4(i_rgb[0]),
        .I5(i_rgb[2]),
        .O(\o_gray[3]_INST_0_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF656465640000)) 
    \o_gray[3]_INST_0_i_3 
       (.I0(i_rgb[1]),
        .I1(i_rgb[3]),
        .I2(i_rgb[0]),
        .I3(i_rgb[2]),
        .I4(final_g[2]),
        .I5(mult_r[12]),
        .O(\o_gray[3]_INST_0_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \o_gray[4]_INST_0 
       (.I0(\o_gray[5]_INST_0_i_3_n_0 ),
        .I1(\o_gray[5]_INST_0_i_1_n_0 ),
        .I2(\o_gray[5]_INST_0_i_2_n_0 ),
        .O(o_gray[4]));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \o_gray[5]_INST_0 
       (.I0(\o_gray[5]_INST_0_i_1_n_0 ),
        .I1(\o_gray[5]_INST_0_i_2_n_0 ),
        .I2(\o_gray[5]_INST_0_i_3_n_0 ),
        .I3(mult_r[15]),
        .I4(final_g[5]),
        .I5(\o_gray[5]_INST_0_i_4_n_0 ),
        .O(o_gray[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h99966666)) 
    \o_gray[5]_INST_0_i_1 
       (.I0(mult_r[14]),
        .I1(final_g[4]),
        .I2(i_rgb[2]),
        .I3(i_rgb[1]),
        .I4(i_rgb[3]),
        .O(\o_gray[5]_INST_0_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFAA58AA580000)) 
    \o_gray[5]_INST_0_i_2 
       (.I0(i_rgb[2]),
        .I1(i_rgb[0]),
        .I2(i_rgb[3]),
        .I3(i_rgb[1]),
        .I4(final_g[3]),
        .I5(mult_r[13]),
        .O(\o_gray[5]_INST_0_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hEAFEA8EAA8EA80A8)) 
    \o_gray[5]_INST_0_i_3 
       (.I0(\o_gray[3]_INST_0_i_2_n_0 ),
        .I1(mult_r[12]),
        .I2(final_g[2]),
        .I3(o_gray_5_sn_1),
        .I4(\o_gray[5]_INST_0_i_6_n_0 ),
        .I5(\o_gray[5]_INST_0_i_7_n_0 ),
        .O(\o_gray[5]_INST_0_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hFFA8A800)) 
    \o_gray[5]_INST_0_i_4 
       (.I0(i_rgb[3]),
        .I1(i_rgb[1]),
        .I2(i_rgb[2]),
        .I3(final_g[4]),
        .I4(mult_r[14]),
        .O(\o_gray[5]_INST_0_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h6900000069696900)) 
    \o_gray[5]_INST_0_i_6 
       (.I0(o_gray_2_sn_1),
        .I1(final_g[1]),
        .I2(mult_r[11]),
        .I3(mult_r[10]),
        .I4(final_g[0]),
        .I5(o_gray_1_sn_1),
        .O(\o_gray[5]_INST_0_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF999899980000)) 
    \o_gray[5]_INST_0_i_7 
       (.I0(i_rgb[0]),
        .I1(i_rgb[3]),
        .I2(i_rgb[1]),
        .I3(i_rgb[2]),
        .I4(final_g[1]),
        .I5(mult_r[11]),
        .O(\o_gray[5]_INST_0_i_7_n_0 ));
  LUT5 #(
    .INIT(32'h69969696)) 
    \o_gray[6]_INST_0 
       (.I0(\o_gray[7]_INST_0_i_1_n_0 ),
        .I1(mult_r[16]),
        .I2(final_g[6]),
        .I3(final_g[5]),
        .I4(mult_r[15]),
        .O(o_gray[6]));
  LUT6 #(
    .INIT(64'h807F15EA15EA7F80)) 
    \o_gray[7]_INST_0 
       (.I0(\o_gray[7]_INST_0_i_1_n_0 ),
        .I1(final_g[5]),
        .I2(mult_r[15]),
        .I3(final_g[7]),
        .I4(final_g[6]),
        .I5(mult_r[16]),
        .O(o_gray[7]));
  LUT6 #(
    .INIT(64'hF6F6F660F6606060)) 
    \o_gray[7]_INST_0_i_1 
       (.I0(final_g[5]),
        .I1(mult_r[15]),
        .I2(\o_gray[5]_INST_0_i_4_n_0 ),
        .I3(\o_gray[5]_INST_0_i_3_n_0 ),
        .I4(\o_gray[5]_INST_0_i_2_n_0 ),
        .I5(\o_gray[5]_INST_0_i_1_n_0 ),
        .O(\o_gray[7]_INST_0_i_1_n_0 ));
  CARRY8 \o_gray[7]_INST_0_i_2 
       (.CI(1'b0),
        .CI_TOP(1'b0),
        .CO({\NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED [7],mult_r[16],\NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED [5],\o_gray[7]_INST_0_i_2_n_3 ,\o_gray[7]_INST_0_i_2_n_4 ,\o_gray[7]_INST_0_i_2_n_5 ,\o_gray[7]_INST_0_i_2_n_6 ,\o_gray[7]_INST_0_i_2_n_7 }),
        .DI({1'b0,1'b0,1'b0,1'b0,i_rgb[9:8],1'b0,i_rgb[8]}),
        .O({\NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED [7:6],mult_r[15:11],\NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED [0]}),
        .S({1'b0,1'b1,i_rgb[11:10],\o_gray[7]_INST_0_i_3_n_0 ,\o_gray[7]_INST_0_i_4_n_0 ,i_rgb[9],\o_gray[7]_INST_0_i_5_n_0 }));
  LUT2 #(
    .INIT(4'h6)) 
    \o_gray[7]_INST_0_i_3 
       (.I0(i_rgb[9]),
        .I1(i_rgb[11]),
        .O(\o_gray[7]_INST_0_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \o_gray[7]_INST_0_i_4 
       (.I0(i_rgb[8]),
        .I1(i_rgb[10]),
        .O(\o_gray[7]_INST_0_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \o_gray[7]_INST_0_i_5 
       (.I0(i_rgb[8]),
        .I1(i_rgb[11]),
        .O(\o_gray[7]_INST_0_i_5_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
