-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (lin64) Build 2552052 Fri May 24 14:47:09 MDT 2019
-- Date        : Fri Sep 11 15:25:02 2020
-- Host        : devlaptop running 64-bit Ubuntu 18.04.4 LTS
-- Command     : write_vhdl -force -mode funcsim
--               /home/bsilva/Documentos/UnB/Final-Project/digital_systems/ov7670mcs/ov7670minized_vga/zcu_cmos/zcu_cmos.srcs/sources_1/bd/OV7670_QVGA/ip/OV7670_QVGA_rgb444_to_grayscale_0_0/OV7670_QVGA_rgb444_to_grayscale_0_0_sim_netlist.vhdl
-- Design      : OV7670_QVGA_rgb444_to_grayscale_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xczu7ev-ffvc1156-2-e
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale is
  port (
    o_gray : out STD_LOGIC_VECTOR ( 7 downto 0 );
    i_rgb : in STD_LOGIC_VECTOR ( 11 downto 0 );
    o_gray_5_sp_1 : in STD_LOGIC;
    o_gray_2_sp_1 : in STD_LOGIC;
    o_gray_1_sp_1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale : entity is "rgb444_to_grayscale";
end OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale;

architecture STRUCTURE of OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale is
  signal final_g : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \mult_g__0_carry__0_i_10_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_11_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_i_9_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_1\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_2\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_3\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_4\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_5\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_6\ : STD_LOGIC;
  signal \mult_g__0_carry__0_n_7\ : STD_LOGIC;
  signal \mult_g__0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry__1_n_7\ : STD_LOGIC;
  signal \mult_g__0_carry_i_1_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_2_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_3_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_4_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_5_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_6_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_7_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_8_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_i_9_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_n_0\ : STD_LOGIC;
  signal \mult_g__0_carry_n_1\ : STD_LOGIC;
  signal \mult_g__0_carry_n_2\ : STD_LOGIC;
  signal \mult_g__0_carry_n_3\ : STD_LOGIC;
  signal \mult_g__0_carry_n_4\ : STD_LOGIC;
  signal \mult_g__0_carry_n_5\ : STD_LOGIC;
  signal \mult_g__0_carry_n_6\ : STD_LOGIC;
  signal \mult_g__0_carry_n_7\ : STD_LOGIC;
  signal mult_r : STD_LOGIC_VECTOR ( 16 downto 10 );
  signal \o_gray[1]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \o_gray[2]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \o_gray[2]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \o_gray[3]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \o_gray[3]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \o_gray[3]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_2_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_6_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_7_n_0\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_2_n_3\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_2_n_4\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_2_n_5\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_2_n_6\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_2_n_7\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_4_n_0\ : STD_LOGIC;
  signal \o_gray[7]_INST_0_i_5_n_0\ : STD_LOGIC;
  signal o_gray_1_sn_1 : STD_LOGIC;
  signal o_gray_2_sn_1 : STD_LOGIC;
  signal o_gray_5_sn_1 : STD_LOGIC;
  signal \NLW_mult_g__0_carry_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_mult_g__0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \NLW_mult_g__0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 1 );
  signal \NLW_mult_g__0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 2 );
  signal \NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 5 );
  signal \NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \o_gray[0]_INST_0_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \o_gray[1]_INST_0\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \o_gray[2]_INST_0\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \o_gray[3]_INST_0_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \o_gray[5]_INST_0_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \o_gray[5]_INST_0_i_4\ : label is "soft_lutpair2";
begin
  o_gray_1_sn_1 <= o_gray_1_sp_1;
  o_gray_2_sn_1 <= o_gray_2_sp_1;
  o_gray_5_sn_1 <= o_gray_5_sp_1;
\mult_g__0_carry\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \mult_g__0_carry_n_0\,
      CO(6) => \mult_g__0_carry_n_1\,
      CO(5) => \mult_g__0_carry_n_2\,
      CO(4) => \mult_g__0_carry_n_3\,
      CO(3) => \mult_g__0_carry_n_4\,
      CO(2) => \mult_g__0_carry_n_5\,
      CO(1) => \mult_g__0_carry_n_6\,
      CO(0) => \mult_g__0_carry_n_7\,
      DI(7) => i_rgb(6),
      DI(6) => \mult_g__0_carry_i_1_n_0\,
      DI(5) => \mult_g__0_carry_i_2_n_0\,
      DI(4) => i_rgb(5),
      DI(3) => i_rgb(7),
      DI(2 downto 0) => B"001",
      O(7 downto 0) => \NLW_mult_g__0_carry_O_UNCONNECTED\(7 downto 0),
      S(7) => \mult_g__0_carry_i_3_n_0\,
      S(6) => \mult_g__0_carry_i_4_n_0\,
      S(5) => \mult_g__0_carry_i_5_n_0\,
      S(4) => \mult_g__0_carry_i_6_n_0\,
      S(3) => \mult_g__0_carry_i_7_n_0\,
      S(2) => \mult_g__0_carry_i_8_n_0\,
      S(1) => \mult_g__0_carry_i_9_n_0\,
      S(0) => i_rgb(4)
    );
\mult_g__0_carry__0\: unisim.vcomponents.CARRY8
     port map (
      CI => \mult_g__0_carry_n_0\,
      CI_TOP => '0',
      CO(7) => \mult_g__0_carry__0_n_0\,
      CO(6) => \mult_g__0_carry__0_n_1\,
      CO(5) => \mult_g__0_carry__0_n_2\,
      CO(4) => \mult_g__0_carry__0_n_3\,
      CO(3) => \mult_g__0_carry__0_n_4\,
      CO(2) => \mult_g__0_carry__0_n_5\,
      CO(1) => \mult_g__0_carry__0_n_6\,
      CO(0) => \mult_g__0_carry__0_n_7\,
      DI(7) => \mult_g__0_carry__0_i_1_n_0\,
      DI(6) => \mult_g__0_carry__0_i_2_n_0\,
      DI(5) => i_rgb(5),
      DI(4) => i_rgb(7),
      DI(3) => i_rgb(4),
      DI(2 downto 1) => i_rgb(6 downto 5),
      DI(0) => \mult_g__0_carry__0_i_3_n_0\,
      O(7 downto 2) => final_g(5 downto 0),
      O(1 downto 0) => \NLW_mult_g__0_carry__0_O_UNCONNECTED\(1 downto 0),
      S(7) => \mult_g__0_carry__0_i_4_n_0\,
      S(6) => \mult_g__0_carry__0_i_5_n_0\,
      S(5) => \mult_g__0_carry__0_i_6_n_0\,
      S(4) => \mult_g__0_carry__0_i_7_n_0\,
      S(3) => \mult_g__0_carry__0_i_8_n_0\,
      S(2) => \mult_g__0_carry__0_i_9_n_0\,
      S(1) => \mult_g__0_carry__0_i_10_n_0\,
      S(0) => \mult_g__0_carry__0_i_11_n_0\
    );
\mult_g__0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => i_rgb(5),
      I1 => i_rgb(7),
      O => \mult_g__0_carry__0_i_1_n_0\
    );
\mult_g__0_carry__0_i_10\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"2D"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(4),
      I2 => i_rgb(5),
      O => \mult_g__0_carry__0_i_10_n_0\
    );
\mult_g__0_carry__0_i_11\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(4),
      I2 => i_rgb(6),
      O => \mult_g__0_carry__0_i_11_n_0\
    );
\mult_g__0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => i_rgb(4),
      I1 => i_rgb(6),
      O => \mult_g__0_carry__0_i_2_n_0\
    );
\mult_g__0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_rgb(4),
      I1 => i_rgb(7),
      O => \mult_g__0_carry__0_i_3_n_0\
    );
\mult_g__0_carry__0_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(5),
      I2 => i_rgb(6),
      O => \mult_g__0_carry__0_i_4_n_0\
    );
\mult_g__0_carry__0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => i_rgb(6),
      I1 => i_rgb(4),
      I2 => i_rgb(7),
      I3 => i_rgb(5),
      O => \mult_g__0_carry__0_i_5_n_0\
    );
\mult_g__0_carry__0_i_6\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => i_rgb(6),
      I1 => i_rgb(4),
      I2 => i_rgb(5),
      O => \mult_g__0_carry__0_i_6_n_0\
    );
\mult_g__0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(5),
      O => \mult_g__0_carry__0_i_7_n_0\
    );
\mult_g__0_carry__0_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(4),
      O => \mult_g__0_carry__0_i_8_n_0\
    );
\mult_g__0_carry__0_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(6),
      O => \mult_g__0_carry__0_i_9_n_0\
    );
\mult_g__0_carry__1\: unisim.vcomponents.CARRY8
     port map (
      CI => \mult_g__0_carry__0_n_0\,
      CI_TOP => '0',
      CO(7 downto 1) => \NLW_mult_g__0_carry__1_CO_UNCONNECTED\(7 downto 1),
      CO(0) => \mult_g__0_carry__1_n_7\,
      DI(7 downto 1) => B"0000000",
      DI(0) => i_rgb(6),
      O(7 downto 2) => \NLW_mult_g__0_carry__1_O_UNCONNECTED\(7 downto 2),
      O(1 downto 0) => final_g(7 downto 6),
      S(7 downto 2) => B"000000",
      S(1) => \mult_g__0_carry__1_i_1_n_0\,
      S(0) => \mult_g__0_carry__1_i_2_n_0\
    );
\mult_g__0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(7),
      O => \mult_g__0_carry__1_i_1_n_0\
    );
\mult_g__0_carry__1_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => i_rgb(6),
      I1 => i_rgb(7),
      O => \mult_g__0_carry__1_i_2_n_0\
    );
\mult_g__0_carry_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(6),
      I1 => i_rgb(4),
      O => \mult_g__0_carry_i_1_n_0\
    );
\mult_g__0_carry_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(5),
      O => \mult_g__0_carry_i_2_n_0\
    );
\mult_g__0_carry_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(5),
      I2 => i_rgb(6),
      O => \mult_g__0_carry_i_3_n_0\
    );
\mult_g__0_carry_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1EE1"
    )
        port map (
      I0 => i_rgb(6),
      I1 => i_rgb(4),
      I2 => i_rgb(7),
      I3 => i_rgb(5),
      O => \mult_g__0_carry_i_4_n_0\
    );
\mult_g__0_carry_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"69"
    )
        port map (
      I0 => i_rgb(5),
      I1 => i_rgb(6),
      I2 => i_rgb(4),
      O => \mult_g__0_carry_i_5_n_0\
    );
\mult_g__0_carry_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(5),
      O => \mult_g__0_carry_i_6_n_0\
    );
\mult_g__0_carry_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => i_rgb(7),
      I1 => i_rgb(4),
      O => \mult_g__0_carry_i_7_n_0\
    );
\mult_g__0_carry_i_8\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(6),
      O => \mult_g__0_carry_i_8_n_0\
    );
\mult_g__0_carry_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => i_rgb(5),
      O => \mult_g__0_carry_i_9_n_0\
    );
\o_gray[0]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666666699999996"
    )
        port map (
      I0 => mult_r(10),
      I1 => final_g(0),
      I2 => i_rgb(1),
      I3 => i_rgb(3),
      I4 => i_rgb(0),
      I5 => i_rgb(2),
      O => o_gray(0)
    );
\o_gray[0]_INST_0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_rgb(8),
      I1 => i_rgb(11),
      O => mult_r(10)
    );
\o_gray[1]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"9F0960F6"
    )
        port map (
      I0 => i_rgb(8),
      I1 => i_rgb(11),
      I2 => final_g(0),
      I3 => o_gray_1_sn_1,
      I4 => \o_gray[1]_INST_0_i_2_n_0\,
      O => o_gray(1)
    );
\o_gray[1]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9999666666669996"
    )
        port map (
      I0 => mult_r(11),
      I1 => final_g(1),
      I2 => i_rgb(2),
      I3 => i_rgb(1),
      I4 => i_rgb(3),
      I5 => i_rgb(0),
      O => \o_gray[1]_INST_0_i_2_n_0\
    );
\o_gray[2]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"396363C6"
    )
        port map (
      I0 => \o_gray[2]_INST_0_i_1_n_0\,
      I1 => \o_gray[2]_INST_0_i_2_n_0\,
      I2 => o_gray_2_sn_1,
      I3 => final_g(1),
      I4 => mult_r(11),
      O => o_gray(2)
    );
\o_gray[2]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF555455540000"
    )
        port map (
      I0 => i_rgb(2),
      I1 => i_rgb(0),
      I2 => i_rgb(3),
      I3 => i_rgb(1),
      I4 => final_g(0),
      I5 => mult_r(10),
      O => \o_gray[2]_INST_0_i_1_n_0\
    );
\o_gray[2]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6666996699996696"
    )
        port map (
      I0 => mult_r(12),
      I1 => final_g(2),
      I2 => i_rgb(2),
      I3 => i_rgb(0),
      I4 => i_rgb(3),
      I5 => i_rgb(1),
      O => \o_gray[2]_INST_0_i_2_n_0\
    );
\o_gray[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \o_gray[3]_INST_0_i_1_n_0\,
      I1 => \o_gray[3]_INST_0_i_2_n_0\,
      I2 => \o_gray[3]_INST_0_i_3_n_0\,
      O => o_gray(3)
    );
\o_gray[3]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A8EA80A8"
    )
        port map (
      I0 => \o_gray[2]_INST_0_i_2_n_0\,
      I1 => mult_r(11),
      I2 => final_g(1),
      I3 => o_gray_2_sn_1,
      I4 => \o_gray[2]_INST_0_i_1_n_0\,
      O => \o_gray[3]_INST_0_i_1_n_0\
    );
\o_gray[3]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9699969669666966"
    )
        port map (
      I0 => mult_r(13),
      I1 => final_g(3),
      I2 => i_rgb(1),
      I3 => i_rgb(3),
      I4 => i_rgb(0),
      I5 => i_rgb(2),
      O => \o_gray[3]_INST_0_i_2_n_0\
    );
\o_gray[3]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF656465640000"
    )
        port map (
      I0 => i_rgb(1),
      I1 => i_rgb(3),
      I2 => i_rgb(0),
      I3 => i_rgb(2),
      I4 => final_g(2),
      I5 => mult_r(12),
      O => \o_gray[3]_INST_0_i_3_n_0\
    );
\o_gray[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"96"
    )
        port map (
      I0 => \o_gray[5]_INST_0_i_3_n_0\,
      I1 => \o_gray[5]_INST_0_i_1_n_0\,
      I2 => \o_gray[5]_INST_0_i_2_n_0\,
      O => o_gray(4)
    );
\o_gray[5]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"17E8E817E81717E8"
    )
        port map (
      I0 => \o_gray[5]_INST_0_i_1_n_0\,
      I1 => \o_gray[5]_INST_0_i_2_n_0\,
      I2 => \o_gray[5]_INST_0_i_3_n_0\,
      I3 => mult_r(15),
      I4 => final_g(5),
      I5 => \o_gray[5]_INST_0_i_4_n_0\,
      O => o_gray(5)
    );
\o_gray[5]_INST_0_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"99966666"
    )
        port map (
      I0 => mult_r(14),
      I1 => final_g(4),
      I2 => i_rgb(2),
      I3 => i_rgb(1),
      I4 => i_rgb(3),
      O => \o_gray[5]_INST_0_i_1_n_0\
    );
\o_gray[5]_INST_0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFAA58AA580000"
    )
        port map (
      I0 => i_rgb(2),
      I1 => i_rgb(0),
      I2 => i_rgb(3),
      I3 => i_rgb(1),
      I4 => final_g(3),
      I5 => mult_r(13),
      O => \o_gray[5]_INST_0_i_2_n_0\
    );
\o_gray[5]_INST_0_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAFEA8EAA8EA80A8"
    )
        port map (
      I0 => \o_gray[3]_INST_0_i_2_n_0\,
      I1 => mult_r(12),
      I2 => final_g(2),
      I3 => o_gray_5_sn_1,
      I4 => \o_gray[5]_INST_0_i_6_n_0\,
      I5 => \o_gray[5]_INST_0_i_7_n_0\,
      O => \o_gray[5]_INST_0_i_3_n_0\
    );
\o_gray[5]_INST_0_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFA8A800"
    )
        port map (
      I0 => i_rgb(3),
      I1 => i_rgb(1),
      I2 => i_rgb(2),
      I3 => final_g(4),
      I4 => mult_r(14),
      O => \o_gray[5]_INST_0_i_4_n_0\
    );
\o_gray[5]_INST_0_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6900000069696900"
    )
        port map (
      I0 => o_gray_2_sn_1,
      I1 => final_g(1),
      I2 => mult_r(11),
      I3 => mult_r(10),
      I4 => final_g(0),
      I5 => o_gray_1_sn_1,
      O => \o_gray[5]_INST_0_i_6_n_0\
    );
\o_gray[5]_INST_0_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF999899980000"
    )
        port map (
      I0 => i_rgb(0),
      I1 => i_rgb(3),
      I2 => i_rgb(1),
      I3 => i_rgb(2),
      I4 => final_g(1),
      I5 => mult_r(11),
      O => \o_gray[5]_INST_0_i_7_n_0\
    );
\o_gray[6]_INST_0\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"69969696"
    )
        port map (
      I0 => \o_gray[7]_INST_0_i_1_n_0\,
      I1 => mult_r(16),
      I2 => final_g(6),
      I3 => final_g(5),
      I4 => mult_r(15),
      O => o_gray(6)
    );
\o_gray[7]_INST_0\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"807F15EA15EA7F80"
    )
        port map (
      I0 => \o_gray[7]_INST_0_i_1_n_0\,
      I1 => final_g(5),
      I2 => mult_r(15),
      I3 => final_g(7),
      I4 => final_g(6),
      I5 => mult_r(16),
      O => o_gray(7)
    );
\o_gray[7]_INST_0_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F6F6F660F6606060"
    )
        port map (
      I0 => final_g(5),
      I1 => mult_r(15),
      I2 => \o_gray[5]_INST_0_i_4_n_0\,
      I3 => \o_gray[5]_INST_0_i_3_n_0\,
      I4 => \o_gray[5]_INST_0_i_2_n_0\,
      I5 => \o_gray[5]_INST_0_i_1_n_0\,
      O => \o_gray[7]_INST_0_i_1_n_0\
    );
\o_gray[7]_INST_0_i_2\: unisim.vcomponents.CARRY8
     port map (
      CI => '0',
      CI_TOP => '0',
      CO(7) => \NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED\(7),
      CO(6) => mult_r(16),
      CO(5) => \NLW_o_gray[7]_INST_0_i_2_CO_UNCONNECTED\(5),
      CO(4) => \o_gray[7]_INST_0_i_2_n_3\,
      CO(3) => \o_gray[7]_INST_0_i_2_n_4\,
      CO(2) => \o_gray[7]_INST_0_i_2_n_5\,
      CO(1) => \o_gray[7]_INST_0_i_2_n_6\,
      CO(0) => \o_gray[7]_INST_0_i_2_n_7\,
      DI(7 downto 4) => B"0000",
      DI(3 downto 2) => i_rgb(9 downto 8),
      DI(1) => '0',
      DI(0) => i_rgb(8),
      O(7 downto 6) => \NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED\(7 downto 6),
      O(5 downto 1) => mult_r(15 downto 11),
      O(0) => \NLW_o_gray[7]_INST_0_i_2_O_UNCONNECTED\(0),
      S(7 downto 6) => B"01",
      S(5 downto 4) => i_rgb(11 downto 10),
      S(3) => \o_gray[7]_INST_0_i_3_n_0\,
      S(2) => \o_gray[7]_INST_0_i_4_n_0\,
      S(1) => i_rgb(9),
      S(0) => \o_gray[7]_INST_0_i_5_n_0\
    );
\o_gray[7]_INST_0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_rgb(9),
      I1 => i_rgb(11),
      O => \o_gray[7]_INST_0_i_3_n_0\
    );
\o_gray[7]_INST_0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_rgb(8),
      I1 => i_rgb(10),
      O => \o_gray[7]_INST_0_i_4_n_0\
    );
\o_gray[7]_INST_0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => i_rgb(8),
      I1 => i_rgb(11),
      O => \o_gray[7]_INST_0_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity OV7670_QVGA_rgb444_to_grayscale_0_0 is
  port (
    i_rgb : in STD_LOGIC_VECTOR ( 11 downto 0 );
    o_gray : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of OV7670_QVGA_rgb444_to_grayscale_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of OV7670_QVGA_rgb444_to_grayscale_0_0 : entity is "OV7670_QVGA_rgb444_to_grayscale_0_0,rgb444_to_grayscale,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of OV7670_QVGA_rgb444_to_grayscale_0_0 : entity is "yes";
  attribute ip_definition_source : string;
  attribute ip_definition_source of OV7670_QVGA_rgb444_to_grayscale_0_0 : entity is "module_ref";
  attribute x_core_info : string;
  attribute x_core_info of OV7670_QVGA_rgb444_to_grayscale_0_0 : entity is "rgb444_to_grayscale,Vivado 2019.1";
end OV7670_QVGA_rgb444_to_grayscale_0_0;

architecture STRUCTURE of OV7670_QVGA_rgb444_to_grayscale_0_0 is
  signal \o_gray[1]_INST_0_i_1_n_0\ : STD_LOGIC;
  signal \o_gray[2]_INST_0_i_3_n_0\ : STD_LOGIC;
  signal \o_gray[5]_INST_0_i_5_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \o_gray[1]_INST_0_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \o_gray[2]_INST_0_i_3\ : label is "soft_lutpair3";
begin
U0: entity work.OV7670_QVGA_rgb444_to_grayscale_0_0_rgb444_to_grayscale
     port map (
      i_rgb(11 downto 0) => i_rgb(11 downto 0),
      o_gray(7 downto 0) => o_gray(7 downto 0),
      o_gray_1_sp_1 => \o_gray[1]_INST_0_i_1_n_0\,
      o_gray_2_sp_1 => \o_gray[2]_INST_0_i_3_n_0\,
      o_gray_5_sp_1 => \o_gray[5]_INST_0_i_5_n_0\
    );
\o_gray[1]_INST_0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF01"
    )
        port map (
      I0 => i_rgb(1),
      I1 => i_rgb(3),
      I2 => i_rgb(0),
      I3 => i_rgb(2),
      O => \o_gray[1]_INST_0_i_1_n_0\
    );
\o_gray[2]_INST_0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0FF1"
    )
        port map (
      I0 => i_rgb(2),
      I1 => i_rgb(1),
      I2 => i_rgb(3),
      I3 => i_rgb(0),
      O => \o_gray[2]_INST_0_i_3_n_0\
    );
\o_gray[5]_INST_0_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F30D"
    )
        port map (
      I0 => i_rgb(2),
      I1 => i_rgb(0),
      I2 => i_rgb(3),
      I3 => i_rgb(1),
      O => \o_gray[5]_INST_0_i_5_n_0\
    );
end STRUCTURE;
