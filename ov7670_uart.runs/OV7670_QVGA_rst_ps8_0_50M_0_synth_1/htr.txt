REM
REM Vivado(TM)
REM htr.txt: a Vivado-generated description of how-to-repeat the
REM          the basic steps of a run.  Note that runme.bat/sh needs
REM          to be invoked for Vivado to track run status.
REM Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
REM

vivado -log OV7670_QVGA_rst_ps8_0_50M_0.vds -m64 -product Vivado -mode batch -messageDb vivado.pb -notrace -source OV7670_QVGA_rst_ps8_0_50M_0.tcl
