/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include "xparameters.h"
#include "visualize.h"

#define IPSTOP		XPAR_COMANDOSTOP_V1_0_0_BASEADDR
#define	IPVSYNC		(XPAR_COMANDOSTOP_V1_0_0_BASEADDR + 0x4)

u8 final_image[TIMG_HEIGTH][TIMG_WIDTH];

int main()
{
    init_platform();

    Xil_Out32(IPSTOP, 0);
    sleep(5);
    Xil_Out32(IPSTOP, 1);
    sleep(1);

    while(Xil_In32(IPVSYNC)!= 1)
    {
    	__asm("nop");
    	usleep(1);
    }

    // Read and print the final image
	Vis_readImage(final_image);

	// Flag to send new image
	xil_printf("imgstart\r\n");
	// Print image properties
	xil_printf("%d,%d,%d,%d\r\n",TIMG_HEIGTH,TIMG_WIDTH,0,0);
	for(int i=0; i < TIMG_HEIGTH; i++)
	{
		for(int j=0; j < TIMG_WIDTH; j++)
		{
			xil_printf("%d, %d, %d\r\n",i,j,final_image[i][j]);
		}
	}

	// Image final package
	xil_printf("imgend\r\n");

    cleanup_platform();
    return 0;
}
