`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: University of Brasilia
// Engineer: Bruno Almeida
// 
// Create Date: 11.09.2020 14:32:00
// Design Name: 
// Module Name: rgb444_to_gray_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module rgb444_to_gray_tb(

    );
//    reg             clk;
    reg     [11:0]  rgb;
    wire    [7:0]   px;
    
//    initial begin
//        clk = 0;
//        forever #5 clk = ~clk;
//    end
    
    initial begin
        rgb = 0;
        #10  rgb    = 12'hfff;
        #10  rgb    = 12'hf7f;
        #10  rgb    = 12'h00f;
        #10  rgb    = 12'h0f0;
        #10  rgb    = 12'hf00;
        $finish;
    end
    
    rgb444_to_grayscale #(
        .gray_length(8)
    )
    dut (
//        .i_clk  (clk),
        .i_rgb  (rgb),
        .o_gray (px )
    );
    
endmodule
