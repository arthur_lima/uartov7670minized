// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Feb  4 14:36:22 2021
// Host        : DESKTOP-967A2H6 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/git/ov7670mcs/ov7670_uart/ov7670_uart.srcs/sources_1/bd/OV7670_QVGA/ip/OV7670_QVGA_ila_0_0/OV7670_QVGA_ila_0_0_stub.v
// Design      : OV7670_QVGA_ila_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z007sclg225-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ila,Vivado 2019.1" *)
module OV7670_QVGA_ila_0_0(clk, probe0)
/* synthesis syn_black_box black_box_pad_pin="clk,probe0[7:0]" */;
  input clk;
  input [7:0]probe0;
endmodule
