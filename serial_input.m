clear all;
close all;

s = serialport(serialportlist,115200);
s.Timeout = 60;


%IMAGEM ESCRITA PELA CAMERA
%
%
k0=zeros(240,320);
for   x = 1:240
    for y = 1:320
    
k0(x,y)=str2double(readline(s));

    end
end 
I0 = mat2gray(k0,[0 256]);
J0 = imrotate(I0,90,'bilinear','crop');
figure(1)
imshow(J0); 
caption = sprintf('Captura QVGA');
title(caption, 'FontSize', 14);
drawnow;
